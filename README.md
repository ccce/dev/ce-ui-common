# CE common UI

This project holds common React hooks and components for CE UI applications.

## Quickstart

```shell
npm ci
```

## Requirements

- NodeJS and npm

# Testing changes against other CE UI repos

In order to test your changes against any of our repos you have to do the following:

1. Create a merge-request
2. Once the pipefile finishes, find the action deploy-package-devel (it's under deploy step) and run it
3. In the build logs look for the "npm notice version", looks something like "9.0.0-27617eb1"
4. Install this version in any of the repos you want to test your changes
