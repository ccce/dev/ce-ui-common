import React, { ReactNode } from 'react';
import { EssThemeProvider } from '../src/providers/EssThemeProvider';
import { theme } from "../src/style/Theme";


interface StoryContext {
  parameters: {
    withThemeProvider?: boolean;
  };
}

interface StoryFn {
  (): ReactNode;
}

export const withThemeProvider = (Story: StoryFn, context: StoryContext) => {
  const { withThemeProvider } = context?.parameters;
  if (withThemeProvider !== false) {
    return (
      <EssThemeProvider theme={theme}>
        <Story />
      </EssThemeProvider>
    );
  }
  return <Story />;
};
