module.exports = {
  "stories": ["../src/**/*.mdx", "../src/**/*.stories.@(js|jsx|ts|tsx)"],

  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",

  ],

  "framework": {
    name: "@storybook/react-vite",
    options: {}
  },

  "core": {
    // 👈 Disables telemetry
    "disableTelemetry": true,
    builder: '@storybook/builder-vite', // 👈 The builder enabled here.
  },
  docs: {},

  typescript: {
    reactDocgen: "react-docgen-typescript"
  }
}
