import { withThemeProvider } from './decorators';

export const decorators = [
  withThemeProvider
];

export const c = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}
