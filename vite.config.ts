import { defineConfig } from "vite";
import dts from "vite-plugin-dts";
import react from "@vitejs/plugin-react-swc";
import { glob } from "glob";
import { libInjectCss } from "vite-plugin-lib-inject-css";
import svgr from "vite-plugin-svgr";
import { peerDependencies } from "./package.json";
import { fileURLToPath } from "node:url";
import { extname, join, relative, resolve } from "node:path";

export default defineConfig({
  build: {
    lib: {
      entry: resolve(__dirname, join("src", "index.js")),
      fileName: "index",
      formats: ["es"]
    },
    sourcemap: true,
    emptyOutDir: true,
    rollupOptions: {
      input: Object.fromEntries(
        glob
          .sync("src/**/*.{js, jsx, ts,tsx}", {
            ignore: [
              "src/setupTests.ts",
              "src/**/*.test.{ts, tsx, js, jsx}",
              "src/**/*.cy.{ts, tsx, js, jsx}",
              "src/stories/*",
              "src/**/*.d.ts",
              "src/**/*.stories.{jsx,tsx}",
              "src/mocks/**/*.js"
            ]
          })
          .map((file) => [
            relative("src", file.slice(0, file.length - extname(file).length)),
            fileURLToPath(new URL(file, import.meta.url))
          ])
      ),
      output: {
        assetFileNames: "assets/[name][extname]",
        chunkFileNames: "chunks/[name].[hash].js",
        entryFileNames: "[name].js"
      },
      external: [...Object.keys(peerDependencies)]
    }
  },
  plugins: [svgr(), react(), dts(), libInjectCss()]
});
