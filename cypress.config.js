import { defineConfig } from "cypress";

export default defineConfig({
  component: {
    viewportHeight: 500,
    viewportWidth: 1000,
    specPattern: [
      'src/**/*.cy.{js,jsx,ts,tsx}'
    ],
    devServer: {
      framework: "react",
      bundler: "vite"
    }
  }
});
