import { KeyboardEvent, useState } from "react";

export const useTypingTimer = ({ init = "", interval = 750 } = {}) => {
  const [typingTimer, setTypingTimer] =
    useState<ReturnType<typeof setTimeout>>();
  const [value, setValue] = useState(init);
  const doneTypingInterval = interval; // ms

  const onKeyUp = (event: KeyboardEvent) => {
    const target = event.target as HTMLInputElement;
    const { key } = event;
    clearTimeout(typingTimer);
    if (key === "Enter") {
      setValue(target.value);
      return;
    } else {
      const timer = setTimeout(
        () => setValue(target.value),
        doneTypingInterval
      );
      setTypingTimer(timer);
    }
  };

  return [value, onKeyUp];
};
