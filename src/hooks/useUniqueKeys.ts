import { nanoid } from "nanoid";
import { useMemo } from "react";

export const useUniqueKeys = (items: unknown[]) => {
  return useMemo(() => {
    if (items) {
      return Array.from({ length: items.length }, () => nanoid());
    }
  }, [items]);
};
