import { useEffect, useRef } from "react";

type Effect = () => void;
type Deps = unknown[];

function useMountEffect(effect: Effect, deps: Deps = [], afterMount: boolean) {
  const didMountRef = useRef(false);

  useEffect(() => {
    let cleanup;
    if (didMountRef.current === afterMount) {
      cleanup = effect();
    }
    didMountRef.current = true;
    return cleanup;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, deps);
}

export function useEffectOnMount(effect: Effect) {
  useMountEffect(effect, [], false);
}

export function useEffectAfterMount(effect: Effect, deps: Deps) {
  useMountEffect(effect, deps, true);
}
