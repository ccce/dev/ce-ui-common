import { useCallback, useMemo, useState } from "react";

const isZeroOrPositiveInt = (val: number) => {
  return Number.isInteger(val) && val >= 0;
};

/**
 * Manages pagination state e.g. outside a table or url.
 *
 * Provides getting/setting for a single pagination object, but internally
 * manages changes for each attribute independently so that e.g. if there are
 * no changes then the object is unchanged.
 */

interface UsePaginationProps {
  initPage: number;
  initLimit: number;
  initTotalCount?: number;
  rowsPerPageOptions?: number[];
}

interface SetPaginationProps {
  page: number;
  limit: number;
  rows: number;
  totalCount: number;
}

const ROWS_PER_PAGE = [20, 50];

export const usePagination = ({
  initPage,
  initLimit,
  initTotalCount,
  rowsPerPageOptions
}: UsePaginationProps) => {
  const [page, setPage] = useState(initPage ?? 0);
  const [_rowsPerPageOptions] = useState(rowsPerPageOptions ?? ROWS_PER_PAGE);
  const [limit, setLimit] = useState(initLimit ?? ROWS_PER_PAGE[0]);
  const [totalCount, setTotalCount] = useState(initTotalCount ?? 0);

  /**
   * Returns current set of pagination values
   */
  const pagination = useMemo(() => {
    const val = {
      page,
      limit,
      rows: limit,
      totalCount,
      totalRecords: totalCount,
      rowsPerPageOptions: _rowsPerPageOptions
    };
    return val;
  }, [page, limit, totalCount, _rowsPerPageOptions]);

  /**
   * Sets pagination. Ignores negative or nonzero values.
   */
  const setPagination = useCallback(
    ({ page, limit, totalCount, rows }: SetPaginationProps) => {
      if (isZeroOrPositiveInt(page)) {
        setPage(page);
      }
      if (isZeroOrPositiveInt(limit || rows)) {
        setLimit(limit || rows);
      }
      if (isZeroOrPositiveInt(totalCount)) {
        setTotalCount(totalCount);
      }
    },
    []
  );

  return {
    pagination,
    setPagination,
    setTotalCount,
    setLimit,
    setPage
  };
};
