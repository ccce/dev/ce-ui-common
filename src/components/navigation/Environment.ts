export enum Environment {
  LOCAL = "LOCAL",
  DEMO = "DEMO",
  TEST = "TEST",
  PROD = "PROD"
}
