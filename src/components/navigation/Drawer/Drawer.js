/**
 * Drawer
 * component utilising Mui Drawer component
 * https://mui.com/material-ui/react-drawer/
 * https://mui.com/material-ui/api/drawer/
 * largely copy and paste from https://mui.com/material-ui/react-drawer/
 */
import { Drawer as MuiDrawer, styled } from "@mui/material";

const openedMixin = (theme) => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen
  }),
  overflowX: "hidden"
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen
  }),
  overflowX: "hidden"
});

export const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) =>
    prop !== "open" && prop !== "widthOpen" && prop !== "widthClosed"
})(({ theme, open, widthOpen, widthClosed }) => ({
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  width: open ? (widthOpen ?? 240) : (widthClosed ?? 80),
  "& > .MuiDrawer-paper": {
    width: open ? (widthOpen ?? 240) : (widthClosed ?? 80)
  },
  "& > .MuiDrawer-paper > .CuiMenuDrawer-Footer > .CuiMenuDrawer-ToggleBox": {
    display: "flex",
    "& > .CuiMenuDrawer-ToggleButton": {
      marginLeft: theme.spacing(1)
    }
  },
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme)
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme)
  })
}));

export const DrawerFooter = styled("div")(({ theme }) => ({
  display: "flex",
  flex: "0 1 auto",
  paddingBottom: theme.spacing(2),
  justifyContent: "flex-start"
}));
