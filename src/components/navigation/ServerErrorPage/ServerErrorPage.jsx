/**
 * ErrorPage
 * when page not found go (redirect) home ("/")
 */
import { string, shape } from "prop-types";
import { ErrorPage } from "../../common/ErrorPage";

const propTypes = {
  /** String communicating the primary problem. Default to "Whoops, you broke the internet" for 5xx, "Not found" otherwise */
  message: string,
  /** HTTP status code (404, 503, etc) */
  status: string,
  /** URL to application support, should the user wish to contact the support desk */
  supportHref: string,
  /** Extra props to override on ErrorPage */
  errorPageProps: shape(ErrorPage.propTypes)
};

export const ServerErrorPage = ({
  message,
  status,
  supportHref,
  errorPageProps
}) => {
  // Define some fallback messages if none provided
  if (!message) {
    if (status?.toString().startsWith("5")) {
      message = "Whoops, looks like you broke the internet 😬";
    } else {
      message = "Page not found";
    }
  }

  return (
    <ErrorPage
      title={message}
      subtitle={status}
      supportHref={supportHref}
      {...errorPageProps}
    />
  );
};

ServerErrorPage.propTypes = propTypes;
