import { Button, styled } from "@mui/material";
import { useCallback } from "react";
import { ErrorBoundary, useErrorHandler } from "react-error-boundary";
import { useEffectOnMount } from "../../../hooks/useMountEffects";
import { theme } from "../../../style/Theme";
import { ErrorPage } from "../../common/ErrorPage";

function ReturnHomeButton({ resetErrorBoundary }) {
  return (
    <Button
      variant="contained"
      onClick={resetErrorBoundary}
      aria-label="Reload Page and navigate home"
    >
      Return to Home
    </Button>
  );
}

function AwSnap({ error, supportHref, resetErrorBoundary }) {
  console.error("AwSnap", error);

  return (
    <ErrorPage
      title="💥 Whoops! Something went wrong 💥"
      titleProps={{ component: "h1", variant: "h2" }}
      details={error.stack}
      supportHref={
        supportHref ?? "https://ess-eric.slack.com/archives/C01V3R31EP2"
      }
      // Navigation etc features don't work after error boundary has been breached
      // So we must override the secondary action with a button that resets the boundary
      SecondaryActionComponent={
        <ReturnHomeButton resetErrorBoundary={resetErrorBoundary} />
      }
    />
  );
}

function WindowErrorRedirect({ children }) {
  const handleError = useErrorHandler();
  const onError = useCallback(
    (event) => {
      handleError(event.error ?? event.reason);
    },
    [handleError]
  );

  useEffectOnMount(() => {
    window.addEventListener("error", onError);
    window.addEventListener("unhandledrejection", onError);
    return function cleanup() {
      window.removeEventListener("error", onError);
      window.removeEventListener("unhandledrejection", onError);
    };
  });

  return children;
}

export const AppErrorBoundary = styled(({ children, supportHref }) => {
  return (
    <ErrorBoundary
      FallbackComponent={(props) => (
        <AwSnap
          supportHref={supportHref}
          {...props}
        />
      )}
      onReset={() => {
        window.location.href = "/";
      }}
    >
      <WindowErrorRedirect>{children}</WindowErrorRedirect>
    </ErrorBoundary>
  );
})(theme);
