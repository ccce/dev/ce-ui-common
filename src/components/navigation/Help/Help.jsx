import { Box, Typography, styled } from "@mui/material";
import { oneOfType, string, element } from "prop-types";
import { ExternalLink } from "../../common/Link/ExternalLink";

const StyledBox = styled(Box)`
  margin-bottom: 1rem;
`;

export const Help = styled(
  ({ summary, docsHref, supportHref, version, apiHref, className }) => {
    return (
      <Box className={className}>
        <StyledBox>
          <Typography variant="h2">About</Typography>
          {typeof summary === "string" ? (
            <Typography>{summary}</Typography>
          ) : (
            summary
          )}
        </StyledBox>
        <StyledBox>
          <Typography variant="h2">Support</Typography>
          <Typography
            display="flex"
            gap={0.5}
          >
            Want more information about this app?
            <ExternalLink
              href={docsHref}
              label="Visit the documentation to learn more"
            >
              Read the docs
            </ExternalLink>
          </Typography>
          <Typography
            display="flex"
            gap={0.5}
          >
            Experiencing issues or want to suggest an improvement?
            <ExternalLink
              href={supportHref}
              label="Report an issue or improvement with the support desk"
            >
              Contact support
            </ExternalLink>
          </Typography>
          <Typography
            display="flex"
            gap={0.5}
          >
            Want to use the API?
            <ExternalLink
              href={apiHref}
              label="REST API documentation"
            >
              REST API documentation
            </ExternalLink>
          </Typography>
        </StyledBox>
        <StyledBox sx={{ display: "flex" }}>
          <Typography fontWeight="bold">Version:</Typography>
          <Typography>&nbsp;{version}</Typography>
        </StyledBox>
      </Box>
    );
  }
)({});

Help.propTypes = {
  summary: oneOfType([string, element]),
  docsHref: string,
  supportHref: string,
  version: string
};
