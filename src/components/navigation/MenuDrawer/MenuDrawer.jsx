import { ChevronLeft, ChevronRight } from "@mui/icons-material";
import { Box, IconButton, styled, Toolbar, Tooltip } from "@mui/material";
import { ContentBox } from "../../common/AppLayout";
import { Drawer, DrawerFooter } from "../Drawer";

export const MenuDrawer = styled(
  ({
    open,
    toggleDrawer,
    widthOpen,
    widthClosed,
    children,
    className,
    ...delegated
  }) => {
    return (
      <Drawer
        className={`${className} CuiMenuDrawer`}
        variant="permanent"
        open={open}
        widthOpen={widthOpen}
        widthClosed={widthClosed}
        onClose={toggleDrawer}
        ModalProps={{ keepMounted: true }} // better performance on mobile
        {...delegated}
      >
        <Toolbar className="CuiMenuDrawer-Spacer" />
        <ContentBox className="CuiMenuDrawer-MenuItemsBox">
          {children}
        </ContentBox>
        <DrawerFooter className="CuiMenuDrawer-Footer">
          <Box className="CuiMenuDrawer-ToggleBox">
            <Tooltip
              title={open ? "Collapse sidebar" : "Expand sidebar"}
              placement="right"
              arrow
            >
              <IconButton
                className="CuiMenuDrawer-ToggleButton"
                onClick={toggleDrawer}
              >
                {open ? <ChevronLeft /> : <ChevronRight />}
              </IconButton>
            </Tooltip>
          </Box>
        </DrawerFooter>
      </Drawer>
    );
  }
)({});
