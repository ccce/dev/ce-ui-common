/**
 * Global App Bar
 * component utilising:
 * Mui Toolbar: https://mui.com/material-ui/api/toolbar/#main-content
 * AppBox, BodyBox, ContentBox, HeaderBox, ButtonAppBar, MenuDrawer
 */
import { styled, Toolbar, Theme } from "@mui/material";
import { ReactNode, useCallback, useState, useEffect } from "react";
import { AppBox, BodyBox, ContentBox, HeaderBox } from "../../common/AppLayout";
import { ButtonAppBar } from "../ButtonAppBar";
import { MenuDrawer } from "../MenuDrawer";
import { Environment } from "../Environment";
import { GlobalAppBarContext } from "../../../contexts/GlobalAppBarContext";

export interface GlobalAppBarProps {
  defaultHomeButton: ReactNode;
  defaultActionButton: ReactNode;
  defaultIconMenuButton: ReactNode;
  defaultHelpButton: ReactNode;
  defaultOpen: boolean;
  defaultTitle: string;
  menuItems: ReactNode[];
  widthOpen: string;
  widthClosed: string;
  environment: Environment | undefined;
  children: ReactNode;
  className: string;
  getDrawerOpen: (value: boolean) => void;
}

interface StyleProps {
  theme: Theme;
}

const defaultStyle = ({ theme }: StyleProps) => ({
  display: "flex",
  "& > .CeuiGlobalAppBar-Header": {
    zIndex: theme.zIndex.drawer + 1
  },
  "& > .CeuiGlobalAppBar-Body > .CeuiGlobalAppBar-ContentBox": {
    padding: theme.spacing(3),
    alignItems: "center"
  },
  "& > .CeuiGlobalAppBar-Body > .CeuiGlobalAppBar-MenuBox": {
    flex: "0 1 auto"
  }
});

export const GlobalAppBar = styled(
  ({
    defaultHomeButton,
    defaultActionButton,
    defaultIconMenuButton,
    defaultHelpButton,
    defaultTitle,
    defaultOpen,
    menuItems,
    widthOpen,
    widthClosed,
    children,
    className,
    getDrawerOpen,
    environment,
    ...delegated
  }: GlobalAppBarProps) => {
    const [title, setTitle] = useState(defaultTitle);
    const setBrowserAndLocalTitle = (newTitle: string) => {
      document.title = newTitle;
      setTitle(newTitle);
    };

    const [value] = useState({
      title: title,
      setTitle: setBrowserAndLocalTitle
    });
    const [drawerOpen, setDrawerOpen] = useState(defaultOpen);

    const toggleDrawer = useCallback(() => {
      setDrawerOpen(!drawerOpen);
    }, [drawerOpen]);

    useEffect(() => {
      getDrawerOpen(drawerOpen);
    }, [drawerOpen, getDrawerOpen]);

    return (
      <AppBox
        className={`${className} CeuiGlobalAppBar`}
        {...delegated}
      >
        <HeaderBox className="CeuiGlobalAppBar-Header">
          <ButtonAppBar
            {...{
              defaultHomeButton,
              defaultActionButton,
              defaultIconMenuButton,
              defaultHelpButton,
              title,
              environment
            }}
          />
          <Toolbar className="CeuiGlobalAppBar-Spacer" />
        </HeaderBox>
        <BodyBox className="CeuiGlobalAppBar-Body">
          <ContentBox className="CeuiGlobalAppBar-MenuBox">
            <MenuDrawer
              {...{
                open: drawerOpen,
                toggleDrawer,
                widthOpen,
                widthClosed
              }}
            >
              {menuItems}
            </MenuDrawer>
          </ContentBox>
          <ContentBox
            className="CeuiGlobalAppBar-ContentBox"
            sx={{
              width: "100%"
            }}
          >
            <GlobalAppBarContext.Provider value={value}>
              {children}
            </GlobalAppBarContext.Provider>
          </ContentBox>
        </BodyBox>
      </AppBox>
    );
  },
  {
    name: "GlobalAppBar",
    slot: "Root"
  }
)(defaultStyle);
