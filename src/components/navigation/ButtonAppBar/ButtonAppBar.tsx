/**
 * Button App Bar
 * component utilising Mui App Bar with buttons component
 * https://mui.com/material-ui/react-app-bar/
 * https://mui.com/material-ui/api/app-bar/
 */
import { ReactNode } from "react";
import { AppBar, styled, Toolbar, Typography, Theme } from "@mui/material";
import { Environment } from "../Environment";

interface StyleProps {
  theme: Theme;
}

interface ButtonAppBarProps {
  title: string;
  defaultActionButton: ReactNode;
  defaultHomeButton: ReactNode;
  defaultIconMenuButton: ReactNode;
  defaultHelpButton: ReactNode;
  environment: Environment | undefined;
}

const defaultStyle = ({ theme }: StyleProps) => ({
  "& > .CeuiButtonAppBar-Toolbar": {
    "& > .CeuiButtonAppBar-HomeBox": {
      marginRight: theme.spacing(2)
    },
    "& > .CeuiButtonAppBar-Title": {
      fontSize: "1.2rem",
      flexGrow: "1",
      overflow: "visible"
    }
  }
});

export const ButtonAppBar = styled(
  ({
    title,
    defaultActionButton,
    defaultHomeButton,
    defaultIconMenuButton,
    defaultHelpButton,
    environment = Environment.PROD,
    ...delegated
  }: ButtonAppBarProps) => {
    return (
      <AppBar
        className="CeuiButtonAppBar"
        position="fixed"
        {...delegated}
      >
        <Toolbar
          className="CeuiButtonAppBar-Toolbar"
          sx={{
            background: (theme) =>
              `${environment === "PROD" ? theme.palette.environment.production.main : theme.palette.environment.development.main}`
          }}
        >
          <div className="CeuiButtonAppBar-HomeBox">{defaultHomeButton}</div>
          <Typography
            variant="h1"
            className="CeuiButtonAppBar-Title"
            noWrap
          >
            {title}
          </Typography>
          {defaultIconMenuButton && <>{defaultIconMenuButton}</>}
          {defaultHelpButton && <>{defaultHelpButton}</>}
          <div className="CeuiButtonAppBar-ActionBox">
            {defaultActionButton}
          </div>
        </Toolbar>
      </AppBar>
    );
  },
  {
    name: "ButtonAppBar",
    slot: "Root"
  }
)(defaultStyle);
