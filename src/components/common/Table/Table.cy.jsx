import { composeStories } from "@storybook/react";
import * as stories from "../../../stories/common/Table/Table.stories";

const { Default } = composeStories(stories);

describe("Table.cy.js", () => {
  context("Given a large amount results", () => {
    it("Should have the expected pagination controls", () => {
      // when mounted
      cy.mount(<Default storybookEnablePagination />);

      // Then the total number of elements is correct
      cy.findByText(/showing .* of 997/i).should("exist");

      // And we have the expected rows per page
      cy.findByRole("combobox", { name: /rows per page/i }).click();
      cy.findAllByRole("option", { name: /^\d{1,2}$/ })
        .should("have.length", 4)
        .then((elems) => {
          cy.wrap(elems[0]).should("contain.text", "5");
          cy.wrap(elems[1]).should("contain.text", "10");
          cy.wrap(elems[2]).should("contain.text", "20");
          cy.wrap(elems[3]).should("contain.text", "50");

          // And when we change the page size to 50
          cy.wrap(elems[3]).click();
        });

      // Then we now have 50 data rows, and 1 header row
      cy.findAllByRole("row").should("have.length", 51);
    });
  });
});
