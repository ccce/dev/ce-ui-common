/**
 * Table
 * component utilising Mui Table (Pro license) component
 * https://mui.com/x/react-data-grid/
 * https://mui.com/x/api/data-grid/data-grid-pro/
 */
import {
  Box,
  LinearProgress,
  Skeleton,
  styled,
  Typography
} from "@mui/material";
import { arrayOf, array, func, string, bool, shape, number } from "prop-types";
import {
  DataGridPro,
  GridToolbarContainer,
  GridToolbarFilterButton,
  GridColumnMenu
} from "@mui/x-data-grid-pro";
import { ess } from "../../../style/Theme";

const propTypes = {
  columns: arrayOf(
    shape({
      field: string.isRequired
    })
  ),
  rows: array.isRequired,
  loading: bool,
  loadingRowCount: number,
  rowClassName: func,
  pagination: shape({
    paginator: bool,
    rowsPerPageOptions: arrayOf(number).isRequired,
    rows: number.isRequired,
    totalRecords: number.isRequired
  }),
  className: string,
  dataTour: string,
  disableHover: bool,
  striped: bool,
  enableFilterToolbar: bool
};

const defaultPageSizeOptions = [20, 50];

const defaultStyle = ({ theme, striped = true, disableHover = true }) => {
  return {
    "&": {
      fontFamily: theme.typography.body1.fontFamily,
      fontSize: theme.typography.body1.fontSize
    },
    // Style column headers
    "& .MuiDataGrid-columnHeaderTitle": {
      fontWeight: "600 !important"
    },
    "& .MuiDataGrid-columnSeparator": {
      display: "none"
    },
    // Disable cell selection outline
    "& .MuiDataGrid-cell:focus-within, & .MuiDataGrid-columnHeader": {
      outline: "none !important"
    },
    "& .MuiDataGrid-row.Mui-hovered": {
      backgroundColor: "transparent"
    },
    ".MuiDataGrid-row": {
      backgroundColor: ess.white,
      "&:hover": {
        backgroundColor: disableHover || striped ? ess.white : ess.lightgrey
      },
      "&.odd": {
        backgroundColor: striped ? ess.lightgrey : ess.white,
        "&:hover": {
          backgroundColor:
            striped || (!striped && !disableHover) ? ess.lightgrey : "inherit"
        }
      }
    },
    // Horizontally center the footer / pagination elements
    "& .MuiDataGrid-footerContainer": {
      justifyContent: "center"
    }
  };
};

const getRowClassName = (params, rowClassName) => {
  // Add striping classes
  let result = params.indexRelativeToCurrentPage % 2 === 0 ? "even" : "odd";

  // Add custom row classes
  if (rowClassName) {
    result += ` ${rowClassName(params) || ""}`;
  }

  return result;
};

const NoRowsOverlay = (props) => {
  return (
    <Box sx={{ padding: 1 }}>
      <Typography
        {...props}
        variant="body1"
      >
        No records found
      </Typography>
    </Box>
  );
};

const LoadingOverlay = (props) => {
  return (
    <LinearProgress
      sx={{ marginY: 2, top: -9 }}
      {...props}
    />
  );
};

export const FilterOnlyToolbar = () => {
  return (
    <GridToolbarContainer>
      <GridToolbarFilterButton />
    </GridToolbarContainer>
  );
};

export const FilterOnlyColumnMenu = (props) => {
  return (
    <GridColumnMenu
      {...props}
      slots={{
        columnMenuSortItem: null,
        columnMenuColumnsItem: null,
        columnMenuPinningItem: null
      }}
    />
  );
};

const defaultRenderCell = (params) => <>{params.value}</>;

export const Table = styled(
  ({
    columns,
    rows = [],
    loading,
    loadingRowCount = 10,
    SkeletonProps,
    rowClassName,
    pagination,
    onPage,
    className,
    dataTour,
    enableFilterToolbar,
    ...props
  }) => {
    // This can always be overriden in the column definition.
    columns = columns.map((config) => ({
      // Automatically cause columns to fill their possible
      // width, otherwise they will not take up the table width
      flex: 1,
      // If a cell value appears to be a React component, then
      // render it; otherwise, render as text/typography
      renderCell: defaultRenderCell,
      // Disable sorting, resizing, and column reordering by default
      sortable: false,
      resizable: false,
      disableReorder: true,
      disableColumnMenu: true,
      filterable: false,
      ...config
    }));

    const showPagination = pagination && (pagination.paginator ?? true);
    const pageSizeOptions =
      pagination?.rowsPerPageOptions || defaultPageSizeOptions;

    const essToMuiPagination = (essModel) => {
      return {
        page: essModel?.page || 0,
        pageSize: essModel?.limit || 5
      };
    };

    const muiToEssPagination = (muiModel) => {
      return {
        page: muiModel?.page || 0,
        limit: muiModel.pageSize || pageSizeOptions[0],
        rowsPerPageOptions: pageSizeOptions
      };
    };

    const onPaginationModelChange = (muiModel) => {
      // Check if the page size changed
      const hasPageSizeChanged =
        muiModel.pageSize !== (pagination.limit || pagination.rows);

      // Reset page to 0 if the page size changed
      const modifiedMuiModel = hasPageSizeChanged
        ? { ...muiModel, page: 0 }
        : muiModel;

      onPage(muiToEssPagination(modifiedMuiModel));
    };

    const labelDisplayedRows = ({ from, to, count }) => {
      return `Showing ${from} to ${to} of ${
        count !== -1 ? count : `more than ${to}`
      }`;
    };

    // When data is loading, show table as populated with skeleton cells
    const finalRows = [];
    const finalColumns = [];
    if (loading && (!rows || rows.length === 0)) {
      // Ensure skeleton still renders as expected even if column
      // render cell has been overridden
      for (const column of columns) {
        finalColumns.push({ ...column, renderCell: defaultRenderCell });
      }

      // Load as many rows as dictated by pagination settings
      // Otherwise use the loading row count
      const rowCount = pagination?.rows || loadingRowCount;
      const loadingRow = columns.reduce((result, curr) => {
        result[curr.field] = (
          <Skeleton
            width="100%"
            {...SkeletonProps}
          />
        );
        return result;
      }, {});
      for (let i = 0; i < rowCount; i++) {
        finalRows.push({ ...loadingRow, id: i });
      }
    } else {
      finalColumns.push(...columns);
      finalRows.push(...rows);
    }

    return (
      <DataGridPro
        data-tour={dataTour} // data attributes don't render, see https://github.com/mui/mui-x/issues/8605
        className={className}
        rows={finalRows}
        columns={finalColumns}
        loading={loading && rows && rows.length > 0}
        getRowClassName={(params) => getRowClassName(params, rowClassName)}
        slots={{
          noRowsOverlay: NoRowsOverlay,
          noResultsOverlay: NoRowsOverlay,
          loadingOverlay: LoadingOverlay,
          toolbar: enableFilterToolbar ? FilterOnlyToolbar : null,
          columnMenu: FilterOnlyColumnMenu,
          ...props?.slots
        }}
        slotProps={{
          pagination: {
            showFirstButton: true,
            showLastButton: true,
            variant: "outlined",
            shape: "rounded",
            labelDisplayedRows: labelDisplayedRows
          },
          ...props.slotProps
        }}
        paginationMode="server"
        rowCount={pagination?.totalCount || 0}
        pagination={showPagination}
        pageSizeOptions={pageSizeOptions}
        paginationModel={essToMuiPagination(pagination)}
        onPaginationModelChange={onPaginationModelChange}
        hideFooter={!showPagination}
        disableMultipleRowSelection
        disableRowSelectionOnClick
        autoHeight
        {...props}
      />
    );
  }
)(defaultStyle);

Table.propTypes = propTypes;
