import {
  Dialog,
  ConfirmationDialog,
  ConfirmDangerActionDialog,
  DialogProps,
  ConfirmationDialogProps,
  ConfirmDangerActionDialogProps
} from "./Dialog";

export {
  Dialog,
  ConfirmationDialog,
  ConfirmDangerActionDialog,
  type DialogProps,
  type ConfirmationDialogProps,
  type ConfirmDangerActionDialogProps
};
