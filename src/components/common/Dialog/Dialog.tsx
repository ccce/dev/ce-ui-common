import { ReactNode, useCallback, useState } from "react";
import {
  Button,
  Dialog as MuiDialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  IconButton,
  styled,
  Box,
  Stack,
  Typography,
  Grid,
  TextField,
  LinearProgress,
  Alert,
  DialogProps as DialogPropsMUI
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";

interface DialogTitleProps {
  onClose: () => void;
  className?: string;
  children: ReactNode | string;
}

const DialogTitle = ({ onClose, className, children }: DialogTitleProps) => (
  <Stack
    className={className}
    flexDirection="row"
    justifyContent="space-between"
    alignItems="center"
    sx={{
      backgroundColor: "primary.main",
      color: "essWhite.main",
      paddingX: 2,
      paddingY: 1
    }}
  >
    {typeof children === "string" ? (
      <Typography>{children}</Typography>
    ) : (
      <>{children}</>
    )}
    <IconButton
      onClick={onClose}
      aria-label="Close Dialog"
      edge="end"
    >
      <CloseIcon
        fontSize="medium"
        sx={{ color: "essWhite.main" }}
      />
    </IconButton>
  </Stack>
);

/**
 * A generic MUI Dialog stylized for ESS that provides:
 *  - a colorful header (uses primary color) with a close button
 *  - a content area
 *  - an action/buttons area, with access to the onClose callback
 *
 * See:
 * https://mui.com/material-ui/react-dialog/
 * https://mui.com/material-ui/api/dialog/
 *
 * NOTE: If you need to provide a confirmation (yes/no) experience,
 * the ConfirmationDialog offers these features already.
 *
 * @param {string|object} title - Title string or custom component
 * @param {string|object} content - Content string or custom component
 * @param {renderActionsCallback} renderActions - render function that receives the onClose callback and returns the dialog footer/actions
 * @param {function} onClose callback fired when the component requests to be closed
 * @param {object} DialogProps MUI Dialog component props; this might be how you change the
 * width of the modal via fullWidth=true, maxWidth="md", for example. See MUI documentation.
 * @param {string} className containing css classname
 */

export interface DialogProps {
  title: string | ReactNode;
  content: string | ReactNode;
  DialogProps: DialogPropsMUI;
  className?: string;
  isLoading?: boolean;
  error?: string;
  onClose: () => void;
  renderActions?: (onClose: () => void) => ReactNode;
}

export const Dialog = ({
  title,
  content,
  className,
  isLoading,
  error,
  DialogProps,
  renderActions,
  onClose
}: DialogProps) => {
  return (
    <MuiDialog
      onClose={onClose}
      fullWidth
      maxWidth="sm"
      {...DialogProps}
    >
      {title ? <DialogTitle onClose={onClose}>{title}</DialogTitle> : null}
      <DialogContent className={className}>
        {typeof content === "string" ? (
          <DialogContentText>{content}</DialogContentText>
        ) : (
          <Box>{content}</Box>
        )}
        {isLoading && <LinearProgress sx={{ marginTop: "16px" }} />}
        {error && (
          <Alert
            severity="error"
            sx={{ marginTop: "8px" }}
          >
            {error}
          </Alert>
        )}
      </DialogContent>
      {renderActions ? (
        <DialogActions>{renderActions(onClose)}</DialogActions>
      ) : null}
    </MuiDialog>
  );
};

interface ConfirmationDialogButtonsProps {
  confirmText: string;
  cancelText: string;
  isConfirmDisabled?: boolean;
  onConfirm: () => void;
  onClose: () => void;
}

const ConfirmationDialogButtons = styled(
  ({
    confirmText,
    cancelText,
    isConfirmDisabled = false,
    onConfirm,
    onClose
  }: ConfirmationDialogButtonsProps) => {
    const handleConfirm = useCallback(() => {
      if (onConfirm) {
        onConfirm();
      }
    }, [onConfirm]);

    const handleClose = useCallback(() => {
      if (onClose) {
        onClose();
      }
    }, [onClose]);

    return (
      <>
        <Button
          className="CeuiConfirmationDialog-CancelButton"
          onClick={handleClose}
          color="primary"
        >
          {cancelText}
        </Button>
        <Button
          className="CeuiConfirmationDialog-ConfirmButton"
          onClick={handleConfirm}
          color="primary"
          variant="contained"
          autoFocus
          disabled={isConfirmDisabled}
        >
          {confirmText}
        </Button>
      </>
    );
  }
)({});

/**
 * An extended ESS Dialog that provides cancel and confirm buttons. Clicking either
 * buttons fires onClose or onConfirm callbacks, and automatically closes the Dialog.
 *
 * NOTE: If you need to control the closing behavior, for example to display a form and
 * only close the dialog once a server request completes, consider extending Dialog
 * on your own.
 *
 * ConfirmationDialog is meant only for simple confirmation from the user!
 *
 * @param {function} onConfirm callback confirming whether confirm button was clicked
 * @param {function} onClose callback confirming whether cancel button was clicked
 * @param {string} confirmText text for confirm button
 * @param {string} cancelText text for cancel button
 * @param {object} ...props - Props passed to the underlying Dialog component (ours, not MUIs).
 * @param {string|object} props.title - Title string or custom component
 * @param {string|object} props.content - Content string or custom component
 * @param {renderActionsCallback} props.renderActions - render function that receives the onClose callback and returns the dialog footer/actions
 * @param {function} props.onClose callback fired when the component requests to be closed
 * @param {object} props.DialogProps MUI Dialog component props; this might be how you change the
 * width of the modal via fullWidth=true, maxWidth="md", for example. See MUI documentation.
 * @param {string} props.className containing css classname
 */

export interface ConfirmationDialogProps extends DialogProps {
  confirmText?: string;
  cancelText?: string;
  isConfirmDisabled?: boolean;
  onConfirm: () => void;
}

export const ConfirmationDialog = styled(
  ({
    title,
    content,
    onConfirm,
    onClose,
    confirmText = "Yes",
    cancelText = "No",
    isConfirmDisabled = false,
    DialogProps
  }: ConfirmationDialogProps) => {
    return (
      <Dialog
        title={title}
        content={content}
        DialogProps={DialogProps}
        onClose={onClose}
        renderActions={() => (
          <ConfirmationDialogButtons
            onConfirm={onConfirm}
            onClose={onClose}
            cancelText={cancelText}
            confirmText={confirmText}
            isConfirmDisabled={isConfirmDisabled}
          />
        )}
      />
    );
  }
)({});

export interface ConfirmDangerActionDialogProps
  extends ConfirmationDialogProps {
  valueToCheck: string;
}

export const ConfirmDangerActionDialog = ({
  title,
  content,
  confirmText,
  valueToCheck,
  DialogProps,
  onClose,
  onConfirm,
  isLoading,
  error
}: ConfirmDangerActionDialogProps) => {
  const [isConfirmDisabled, setIsConfirmDisabled] = useState(true);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const inputText = event.target.value;
    setIsConfirmDisabled(inputText !== valueToCheck);
  };

  const handleClose = () => {
    setIsConfirmDisabled(true);
    onClose();
  };

  return (
    <ConfirmationDialog
      title={title}
      confirmText={confirmText}
      cancelText="Cancel"
      onClose={handleClose}
      DialogProps={DialogProps}
      content={
        <Grid
          container
          spacing={2}
        >
          <Grid
            item
            xs={12}
          >
            {typeof content === "string" ? (
              <DialogContentText>{content}</DialogContentText>
            ) : (
              <Box>{content}</Box>
            )}
          </Grid>
          <Grid
            item
            xs={12}
          >
            <TextField
              autoComplete="off"
              variant="outlined"
              label={valueToCheck}
              fullWidth
              onChange={handleInputChange}
            />
          </Grid>
        </Grid>
      }
      {...{ open, onConfirm, isConfirmDisabled, isLoading, error }}
    />
  );
};
