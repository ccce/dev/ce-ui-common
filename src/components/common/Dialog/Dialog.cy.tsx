import { composeStories } from "@storybook/react";
import { mount } from "cypress/react";
import * as stories from "../../../stories/common/Dialog/Dialog.stories";

const { BasicDialog, Confirmation, DangerActionDialog } =
  composeStories(stories);

describe("SimpleDialog", () => {
  it("should close on clicking close", () => {
    mount(<BasicDialog />);

    cy.findByText(/some title/i).should("exist");
    cy.findByRole("button", { name: /close/i }).click();
    cy.findByText(/some title/i).should("not.exist");
  });
  it("should close on escape", () => {
    mount(<BasicDialog />);

    cy.findByText(/some title/i)
      .should("exist")
      .type("{ESC}");
    cy.findByText(/some title/i).should("not.exist");
  });
});

const closedText = "You closed the dialog!";

describe("ConfirmationDialog", () => {
  it("should close on clicking close icon", () => {
    mount(<Confirmation />);

    cy.findByText(/some title/i).should("exist");
    cy.findByRole("button", { name: /close/i }).click();
    cy.findByText(/some title/i).should("not.exist");
    cy.findByText(closedText).should("exist");
  });
  it("should close on escape", () => {
    mount(<Confirmation />);

    cy.findByText(/some title/i)
      .should("exist")
      .type("{ESC}");
    cy.findByText(/some title/i).should("not.exist");
    cy.findByText(closedText).should("exist");
  });
  it("should close and when clicking cancel", () => {
    mount(<Confirmation />);

    cy.findByText(/some title/i).should("exist");
    cy.findByRole("button", { name: /cancel/i }).click();
    cy.findByText(/some title/i).should("not.exist");
    cy.findByText(closedText).should("exist");
  });
});

describe("DangerActionDialog", () => {
  const mountDangerActionDialog = () => {
    mount(<DangerActionDialog />);
    cy.contains("Open Danger Action Dialog").click();
  };

  it("should open with disabled confirm button", () => {
    mountDangerActionDialog();
    cy.findByRole("button", { name: /delete/i }).should("be.disabled");
  });
  it("should disable confirm button if entered value does not match value to check against", () => {
    mountDangerActionDialog();
    cy.findByRole("textbox", { name: /ec234-67hdj/i }).type("ec234-67hd");
    cy.findByRole("button", { name: /delete/i }).should("be.disabled");
  });
  it("should enable confirm button if entered value matches value to check against", () => {
    mountDangerActionDialog();
    cy.findByRole("textbox", { name: /ec234-67hdj/i }).type("ec234-67hdj");
    cy.findByRole("button", { name: /delete/i }).should("be.enabled");
  });
  it("should disable confirm button clear text field on cancel", () => {
    mountDangerActionDialog();
    cy.findByRole("textbox", { name: /ec234-67hdj/i }).type("ec234-67hdj");
    cy.findByRole("button", { name: /delete/i }).should("be.enabled");
    cy.findByRole("button", { name: /cancel/i }).click();
    cy.contains("Open Danger Action Dialog").click();
    cy.findByRole("textbox", { name: /ec234-67hdj/i }).should("have.value", "");
    cy.findByRole("button", { name: /delete/i }).should("be.disabled");
  });
  it("should disable confirm button and clear text field on close", () => {
    mountDangerActionDialog();
    cy.findByRole("textbox", { name: /ec234-67hdj/i }).type("ec234-67hdj");
    cy.findByRole("button", { name: /delete/i }).should("be.enabled");
    cy.findByRole("button", { name: /close/i }).click();
    cy.contains("Open Danger Action Dialog").click();
    cy.findByRole("textbox", { name: /ec234-67hdj/i }).should("have.value", "");
    cy.findByRole("button", { name: /delete/i }).should("be.disabled");
  });
});
