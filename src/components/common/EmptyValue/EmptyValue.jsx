/**
 * EmptyValue
 * Component providing consistent grayed out ---
 */
import { Typography } from "@mui/material";

export const EmptyValue = () => <Typography color="gray">---</Typography>;
