/**
 * ExpandableViewer
 * component will expand on contained content to a dialog
 */
import { useState, useCallback } from "react";
import {
  Button,
  Stack,
  Box,
  LinearProgress,
  styled,
  Typography
} from "@mui/material";
import LaunchIcon from "@mui/icons-material/Launch";
import { useWindowDimensions } from "../../../hooks/useWindowDimensions";
import { Dialog } from "../Dialog/Dialog";

const defaultStyle = () => ({
  "&.CeuiExpandableViewer--sticky": {
    position: "sticky",
    top: "76px"
  },
  "& > .CeuiExpandableViewer-ChildrenBox": {
    height: "300px"
  }
});

const FullWidthProgress = styled(LinearProgress)({
  width: "100%"
});

export const ExpandableViewerButton = ({ onClick, expandButtonText }) => {
  return (
    <Button
      color="inherit"
      variant="outlined"
      endIcon={<LaunchIcon />}
      onClick={onClick}
      className="CeuiExpandableViewer-Button"
    >
      {expandButtonText}
    </Button>
  );
};

/**
 * Dialog
 * @param {object} props
 * @param {string} props.title contained content and dialog title
 * @param {string} props.expandButtonText text for button opening dialog
 * @param {function} props.renderTitle A function providing an object containing the
 * title, onClick, and expandButtonText as arguments. Must return a rendered component.
 * @param {boolean} props.loading whether to show progressbar
 * @param {object} props.children child(ren) of ExpandableViewer component
 * @param {string} props.className containing css classname
 * @param {bool} props.sticky make the view sticky (stick) to top
 */
export const ExpandableViewer = styled(
  ({
    title,
    expandButtonText,
    renderTitle,
    loading,
    className,
    children,
    sticky = false,
    DialogProps
  }) => {
    const [dialogOpen, setDialogOpen] = useState(false);
    const { height } = useWindowDimensions();

    const onClick = useCallback(() => {
      setDialogOpen(true);
    }, [setDialogOpen]);

    const handleClose = useCallback(() => {
      setDialogOpen(false);
    }, [setDialogOpen]);

    return (
      <div
        className={`CeuiExpandableViewer ${
          sticky ? "CeuiExpandableViewer--sticky" : ""
        } ${className}`}
      >
        {!dialogOpen && loading ? (
          <FullWidthProgress />
        ) : (
          <>
            <Dialog
              onClose={handleClose}
              title={title}
              content={
                <Box
                  height={height - 200}
                  className="CeuiExpandableViewerDialog-Box"
                >
                  {children}
                </Box>
              }
              className="CeuiExpandableViewerDialog"
              DialogProps={{
                open: dialogOpen,
                fullWidth: true,
                maxWidth: "xl",
                ...DialogProps
              }}
            />
            {renderTitle ? (
              renderTitle({ title, onClick, expandButtonText })
            ) : (
              <Stack
                flexDirection="row"
                justifyContent="space-between"
                alignItems="center"
                className="CeuiExpandableViewer-Grid"
                sx={{ marginBottom: "10px" }}
              >
                <Typography
                  variant="h3"
                  component="p"
                  sx={{ verticalAlign: "center" }}
                >
                  {title}
                </Typography>
                <ExpandableViewerButton {...{ onClick, expandButtonText }} />
              </Stack>
            )}
            <Box
              data-tour="expandable-viewer-children-box"
              className="CeuiExpandableViewer-ChildrenBox"
            >
              {children}
            </Box>
          </>
        )}
      </div>
    );
  }
)(defaultStyle);
