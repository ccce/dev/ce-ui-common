import { Tooltip, Typography } from "@mui/material";
import { string, object, bool, node } from "prop-types";

/**
 * Displays cutoff text with an ellipsis and a tooltip with the rest of the text.
 * @param {string} title text to display in the tooltip. The child element is used
 * by default so title is only necessary if the child is a React component.
 * @param {boolean} disableTooltip disables the tooltip feature.
 * @param {object} TypographyProps Additional props for the Typography component
 * @param {object} TooltipProps Additional props for the Tooltip component
 */
export const EllipsisText = ({
  title,
  disableTooltip = false,
  TypographyProps,
  TooltipProps,
  children
}) => {
  const renderedText = (
    <Typography
      noWrap
      {...TypographyProps}
    >
      {children}
    </Typography>
  );

  if (disableTooltip) {
    return renderedText;
  }

  return (
    <Tooltip
      title={title ?? children}
      arrow
      {...TooltipProps}
    >
      {renderedText}
    </Tooltip>
  );
};
EllipsisText.propTypes = {
  title: string,
  disableTooltip: bool,
  TypographyProps: object,
  TooltipProps: object,
  children: node
};
