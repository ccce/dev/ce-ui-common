/**
 * ErrorPage
 * when page not found go (redirect) home ("/")
 */
import { useState } from "react";
import {
  Typography,
  Box,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  useMediaQuery
} from "@mui/material";
import { element, object, string } from "prop-types";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { ExternalButtonLink, InternalButtonLink } from "../Link";
import { theme } from "../../../style/Theme";

const propTypes = {
  /** String describing the most important informatio about the error */
  title: string,
  /** String describing supporting information about the error */
  subtitle: string,
  /** Additional details about the error message, such as a stacktrace if relevant */
  details: string,
  /** Secondary action component that is rendered instead of the "Go home" navigation link */
  SecondaryActionComponent: element,
  /** Mascot component that is rendered on left of title */
  mascotVariantL: element,
  /** Mascot component that is rendered on right of title */
  mascotVariantR: element,
  /** URL to application support, should the user wish to contact the support desk */
  supportHref: string,
  /** Additional props for the title typography; e.g. to override the variant or component */
  titleProps: object,
  /** Additional props for the subtitle typography; e.g. to override the variant or component */
  subtitleProps: object
};

export const ErrorPage = ({
  title,
  subtitle,
  details,
  SecondaryActionComponent,
  mascotVariantL,
  mascotVariantR,
  supportHref,
  titleProps,
  subtitleProps
}) => {
  const [showDetails, setShowDetails] = useState(false);
  const isDesktop = useMediaQuery(theme.breakpoints.up("sm"));

  const toggleDetails = () => {
    setShowDetails(!showDetails);
  };

  return (
    <>
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        gap={2}
      >
        <Box
          display="flex"
          flexDirection="row"
          alignItems="center"
          gap={2}
        >
          {mascotVariantL}
          <Box
            display="flex"
            flexDirection="column"
            alignItems="center"
            gap={2}
          >
            <Typography
              variant="h3"
              {...subtitleProps}
            >
              {subtitle}
            </Typography>
            <Typography
              variant="h2"
              {...titleProps}
            >
              {title}
            </Typography>
          </Box>
          {mascotVariantR}
        </Box>
        {details ? (
          <Accordion
            expanded={showDetails}
            onChange={toggleDetails}
            sx={{ maxWidth: isDesktop ? "50%" : "100%" }}
          >
            <AccordionSummary
              id="error-details-header"
              aria-controls="error-details-content"
              expandIcon={<ExpandMoreIcon />}
            >
              <Typography variant="body1">
                {showDetails ? "Hide Details" : "Show Details"}
              </Typography>
            </AccordionSummary>
            <AccordionDetails
              sx={{
                whiteSpace: "pre",
                overflow: "scroll"
              }}
            >
              <Typography
                variant="body2"
                fontFamily="monospace"
              >
                {details}
              </Typography>
            </AccordionDetails>
          </Accordion>
        ) : null}
        <Box
          display="flex"
          justifyContent="center"
          gap={2}
        >
          {SecondaryActionComponent ?? (
            <InternalButtonLink
              variant="contained"
              color="primary"
              to="/"
            >
              Return to Home
            </InternalButtonLink>
          )}

          {supportHref ? (
            <ExternalButtonLink
              href={supportHref}
              variant="outlined"
            >
              Contact Support
            </ExternalButtonLink>
          ) : null}
        </Box>
      </Box>
    </>
  );
};

ErrorPage.propTypes = propTypes;
