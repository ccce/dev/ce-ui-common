import { useState, isValidElement } from "react";
import { string, shape, func, element, arrayOf, oneOfType } from "prop-types";
import {
  Button,
  Tooltip,
  Menu,
  MenuItem,
  ListItemText,
  ListItemIcon,
  Box
} from "@mui/material";
import { AddBox, KeyboardArrowDown } from "@mui/icons-material";
import { useUniqueKeys } from "../../../hooks/useUniqueKeys";

const propTypes = {
  menuItems: arrayOf(
    shape({
      text: oneOfType([string, element]),
      action: func,
      icon: element
    })
  ),
  icon: element,
  id: string
};

export const IconMenuButton = ({
  menuItems,
  icon = <AddBox />,
  id,
  className
}) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const menuItemsKeys = useUniqueKeys(menuItems);

  /*
   Check if menuItems prop is an array of one
   Single button without menu must be an array of one object
  */
  const singleItem = menuItems?.length === 1 && id !== "user-login-profile-";

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Tooltip
        title={singleItem ? menuItems[0]?.text : ""}
        arrow
      >
        <Button
          className={className}
          id={`${id}icon-menu-button-button`}
          color="primaryContrastText"
          sx={icon?.props?.alt ? undefined : { mr: 1 }}
          onClick={singleItem ? menuItems[0]?.action : handleClick}
          aria-label={singleItem ? menuItems[0]?.text : undefined}
          aria-controls={anchorEl ? `${id}icon-menu-button-menu` : undefined}
          aria-expanded={
            // eslint-disable-next-line no-nested-ternary
            !singleItem ? (anchorEl ? "true" : "false") : undefined
          }
          aria-haspopup={singleItem ? undefined : true}
        >
          <Box
            sx={{
              mr: singleItem ? undefined : 1,
              maxHeight: (theme) => theme.spacing(3)
            }}
          >
            {icon}
          </Box>
          {!singleItem && <KeyboardArrowDown />}
        </Button>
      </Tooltip>
      {!singleItem && (
        <Menu
          id={`${id}icon-menu-button-menu`}
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
          MenuListProps={{ "aria-labelledby": `${id}icon-menu-button-button` }}
        >
          {menuItems?.map(({ text, action, icon }, i) => (
            <MenuItem
              key={menuItemsKeys[i]}
              disabled={!action}
              onClick={(e) => {
                if (action) {
                  handleClose();
                  action(e);
                }
              }}
              sx={action ? undefined : { opacity: "1.0!important" }}
            >
              {icon && <ListItemIcon>{icon}</ListItemIcon>}
              {isValidElement(text) ? (
                text
              ) : (
                <ListItemText
                  primary={text && text}
                  primaryTypographyProps={{ variant: "button" }}
                />
              )}
            </MenuItem>
          ))}
        </Menu>
      )}
    </>
  );
};

IconMenuButton.propTypes = propTypes;
