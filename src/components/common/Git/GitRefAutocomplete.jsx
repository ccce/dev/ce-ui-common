import {
  Tooltip,
  Box,
  Stack,
  ListItem,
  Typography,
  createFilterOptions
} from "@mui/material";
import LabelOutlinedIcon from "@mui/icons-material/LabelOutlined";
import { array, bool, string, func, object } from "prop-types";
import { Autocomplete } from "../Autocomplete";
import { EllipsisText } from "../EllipsisText";
import { formatRelativeTime, formatDateAndTime } from "../../../lib/Time-Utils";

const propTypes = {
  id: string,
  label: string,
  loading: bool,
  options: array,
  disabled: bool,
  onGitQueryValueSelect: func,
  onGitQueryValueChange: func,
  autocompleteProps: object,
  textFieldProps: object
};

const TAG_TYPE = "TAG";

const boldTextStyle = {
  component: "p",
  variant: "code",
  fontWeight: "bold"
};

const mediumTextStyle = {
  component: "p",
  variant: "code",
  fontSize: "0.8rem",
  color: "primary.mediumText",
  fontWeight: "fontWeightMedium"
};

const styleForCommitMessage = {
  component: "p",
  variant: "code",
  fontSize: "0.8rem",
  fontWeight: "bold"
};

const filterOptions = createFilterOptions({
  matchFrom: "any",
  stringify: (option) =>
    `${option.short_reference}${option.reference}${option.description}`
});

const TRUNCATE_LENGTH = 30;
const truncate = (str) => {
  if (str.length <= TRUNCATE_LENGTH) {
    return str;
  }
  const subString = str.slice(0, TRUNCATE_LENGTH); // the original check
  return subString.slice(0, subString.lastIndexOf(" ")) + "...";
};

const getOptionLabel = (option) => {
  if (option) {
    if (option.type === TAG_TYPE) {
      return option.description === undefined || option.description === ""
        ? option.reference
        : `${option.reference} - ${truncate(option.description)}`;
    }
    return `${truncate(option.description)} - ${option.short_reference}`;
  }
  return "";
};

const isOptionEqualToValue = (option, value) => {
  return option.reference === value.reference;
};

export const GitRefAutocomplete = ({
  id,
  value,
  inputValue,
  label,
  loading,
  options = [],
  disabled,
  onGitQueryValueSelect,
  onGitQueryStringChange,
  autocompleteProps,
  textFieldProps
}) => {
  return (
    <Autocomplete
      loading={loading}
      textFieldProps={{ label, ...textFieldProps }}
      autocompleteProps={{
        id,
        value,
        onChange: onGitQueryValueSelect,
        inputValue,
        onInputChange: onGitQueryStringChange,
        disabled: disabled,
        options: options ?? [],
        filterOptions: filterOptions,
        getOptionLabel: getOptionLabel,
        isOptionEqualToValue: isOptionEqualToValue,
        renderOption: (props, option) => {
          const isTag = option.type === TAG_TYPE;
          return (
            <ListItem
              {...props}
              key={isTag ? option.reference : option.short_reference}
            >
              <Box sx={{ width: "100%" }}>
                <Stack
                  flexDirection="row"
                  justifyContent="space-between"
                  sx={{ width: "100%", height: "1.5rem" }}
                >
                  {isTag ? (
                    <Box
                      sx={{
                        display: "inline-flex",
                        alignItems: "center",
                        gap: "0.2rem"
                      }}
                    >
                      <LabelOutlinedIcon fontSize="small" />
                      <Typography
                        {...styleForCommitMessage}
                        noWrap
                      >
                        {option.reference}
                      </Typography>
                    </Box>
                  ) : (
                    <Box sx={{ maxWidth: "80%" }}>
                      <EllipsisText
                        TypographyProps={{ ...styleForCommitMessage }}
                      >
                        {option.description}
                      </EllipsisText>
                    </Box>
                  )}
                  <Typography {...boldTextStyle}>
                    {option.short_reference}
                  </Typography>
                </Stack>
                <Box
                  sx={{
                    width: "100%",
                    display: "inline-flex",
                    alignItems: "center",
                    gap: "0.2rem"
                  }}
                >
                  <Tooltip
                    placement="right"
                    title={`${formatDateAndTime(option.commit_date)}`}
                    aria-label={`Commited ${formatRelativeTime(
                      option.commit_date
                    )}, on ${formatDateAndTime(option.commit_date)}`}
                  >
                    <Typography {...mediumTextStyle}>
                      {formatRelativeTime(option.commit_date)}
                    </Typography>
                  </Tooltip>
                  {isTag && option.description !== "" ? (
                    <Box sx={{ width: "70%" }}>
                      <EllipsisText TypographyProps={{ ...mediumTextStyle }}>
                        <span>&nbsp;-&nbsp;</span>
                        {option.description}
                      </EllipsisText>
                    </Box>
                  ) : null}
                </Box>
              </Box>
            </ListItem>
          );
        },
        ...autocompleteProps
      }}
    />
  );
};

GitRefAutocomplete.propTypes = propTypes;
