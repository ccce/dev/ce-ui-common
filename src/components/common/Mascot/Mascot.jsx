import SidewaysBadgerDuck from "./sidewaysbadgerduck.svg?react";
import VerticalBadgerDuck from "./verticalbadgerduck.svg?react";

export function SidewaysMascot(props) {
  return <SidewaysBadgerDuck {...props} />;
}

export function VerticalMascot(props) {
  return <VerticalBadgerDuck {...props} />;
}
