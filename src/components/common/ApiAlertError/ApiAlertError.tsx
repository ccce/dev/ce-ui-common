import { Alert } from "@mui/material";
import { getErrorState, ApiError } from "./AlertsData";

interface ApiAlertErrorProps {
  error: ApiError;
}

export const ApiAlertError = ({ error }: ApiAlertErrorProps) => (
  <Alert severity="error">{getErrorState(error).message}</Alert>
);
