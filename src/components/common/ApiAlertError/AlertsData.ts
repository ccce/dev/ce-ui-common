import { SerializedError } from "@reduxjs/toolkit";
import { FetchBaseQueryError } from "@reduxjs/toolkit/query";

export type ApiError = FetchBaseQueryError | SerializedError | undefined;

export interface ErrorMessage {
  error: string;
  description: string;
}

export interface BrowserError {
  error: string;
  status: string;
}

export const isBrowserError = (
  error: BrowserError | FetchBaseQueryError | SerializedError | unknown
) => {
  return (
    error != null &&
    typeof error === "object" &&
    "error" in error &&
    typeof (error as BrowserError).status === "string"
  );
};

export const isFetchBaseQueryError = (
  error: FetchBaseQueryError | SerializedError | BrowserError | unknown
): error is FetchBaseQueryError => {
  return (
    error != null &&
    typeof error === "object" &&
    "status" in error &&
    typeof (error as FetchBaseQueryError).status === "number"
  );
};

export const isSerializedError = (
  error: FetchBaseQueryError | SerializedError | BrowserError | unknown
): error is SerializedError => {
  return (
    error != null &&
    typeof error === "object" &&
    "message" in error &&
    typeof (error as SerializedError).message === "string"
  );
};

export const getErrorState = (
  error: FetchBaseQueryError | SerializedError | BrowserError | unknown
) => {
  const fallbackMessage = "Unknown error, contact support";

  if (isFetchBaseQueryError(error)) {
    const customError = error.data as ErrorMessage;
    const message = customError.description || customError.error;

    if (customError) {
      return {
        message: message || fallbackMessage,
        status: error.status || 0
      };
    }
  } else if (isSerializedError(error)) {
    return {
      message: error.message || fallbackMessage,
      status: error.code || 0
    };
  } else if (isBrowserError(error)) {
    const browserError = error as BrowserError;
    return {
      message: browserError.error || fallbackMessage,
      status: browserError.status || 0
    };
  }
  return {
    message: fallbackMessage,
    status: 0
  };
};
