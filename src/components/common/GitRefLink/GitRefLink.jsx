/**
 * GitRefLink
 * Component providing link (and tag) to gitlab
 */
import { string } from "prop-types";
import { ExternalLink } from "../Link/ExternalLink";

/**
 * Displays a link to a git reference, or empty value if disabled.
 * @param {string} url String containing url to gitlab template.
 * @param {string} revision String containing gitlab tag
 */
export const GitRefLink = ({ url, revision, ...props }) => {
  // if no git reference has been entered
  if (!url || url.trim === "") {
    return <></>;
  }

  // if user enters GIT url with '.git' postfix -> remove it
  if (url.toLowerCase().endsWith(".git")) {
    url = url.toLowerCase().split(".git")[0];
  }

  // add trailing '/' if needed for GIT server address
  if (!url.endsWith("/")) {
    url += "/";
  }

  // calculate the endpoint for tag/commitId
  url = url + "-/tree/" + revision;

  return (
    <ExternalLink
      href={url}
      label={`Git revision ${revision}`}
      {...props}
    >
      {revision}
    </ExternalLink>
  );
};

GitRefLink.propTypes = {
  url: string,
  revision: string
};
