import { Box, Stack, styled } from "@mui/material";
import { STEPPER_STATES, StepperComponents } from "./StepperData";
import { useUniqueKeys } from "../../../hooks/useUniqueKeys";
import { theme } from "../../../style/Theme";

const describeState = (steps, activeStep, isCompleted) => {
  if (isCompleted) {
    let completedTxt = "Process is finished";
    for (let [index, step] of steps.entries()) {
      completedTxt = completedTxt += `step ${index + 1} ${step.state}`;
    }
    return completedTxt;
  } else {
    if (activeStep < 0) {
      return "Process is queued";
    } else {
      const queued = steps.filter(
        (step) => step.state === STEPPER_STATES.QUEUED
      );
      return `Process is running. ${steps.length - queued.length} of ${
        steps.length
      } steps completed`;
    }
  }
};

const Connector = styled(Box)(() => ({
  width: "100%",
  height: 1,
  borderBottom: `2px solid ${theme.palette.status.disabled.main}`,
  position: "relative",
  top: 9
}));

/**
 * Styled device-responsive Stepper.
 * type Step {
    key: string;
    label: string;
    state: string;
  }
 * @param {Steps[]} steps - List of steps
 * @param {number} activeStep - Index of the current step, indexing from 0 if there is a active step, -1 if there is no active step.
 * @param {boolean} isCompleted - all steps are completed
 * @param {boolean} showToolTip - if true, step label is hidden and tooltip is enabled
 * @returns Linear stepper
 */

export const LinearStepper = ({ steps, activeStep, isCompleted }) => {
  const stepsKeys = useUniqueKeys(steps);
  return (
    <Stack
      role="progressbar"
      flexDirection="row"
      tabIndex={0}
      divider={<Connector className="inline-stepper-connector" />}
      aria-label={describeState(steps, activeStep, isCompleted)}
    >
      {steps.map((step, index) => {
        const StepIcon = StepperComponents[step.state];
        return (
          <StepIcon
            key={stepsKeys[index]}
            step={step}
            showToolTip
          />
        );
      })}
    </Stack>
  );
};
