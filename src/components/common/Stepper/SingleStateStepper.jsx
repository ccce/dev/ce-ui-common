import { Box } from "@mui/material";
import { StepperComponents } from "./StepperData";

/**
 * Styled device-responsive Stepper.
 * type Step {
    label: string;
    state: string;
  }
 * @param {Step} step - Currently active step/state
 * @param {string} bgSize - in px deciding the size of background circle
 * @param {string} iconSize - xsmall | small | medium | large
 * @param {boolean} showToolTip - if true, step label is hidden and tooltip is enabled
 * @returns Stepper
 */

export const SingleStateStepper = ({ step, ...props }) => {
  const StepIcon = StepperComponents[step.state];
  return (
    <Box
      role="progressbar"
      tabIndex={0}
      aria-label={step.label}
    >
      <StepIcon
        step={step}
        {...props}
      />
    </Box>
  );
};
