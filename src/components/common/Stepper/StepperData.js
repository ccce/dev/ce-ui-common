import {
  StepQueued,
  StepFailed,
  StepRunning,
  StepSkipped,
  StepSuccessful,
  StepUnknown
} from "./Steps";

export const STEPPER_STATES = {
  QUEUED: "QUEUED",
  RUNNING: "RUNNING",
  FAILED: "FAILED",
  PARTIAL_FAILURE: "PARTIAL_FAILURE",
  SKIPPED: "SKIPPED",
  SUCCESSFUL: "SUCCESSFUL",
  UNKNOWN: "UNKNOWN"
};

export const StepperComponents = {
  QUEUED: StepQueued,
  RUNNING: StepRunning,
  FAILED: StepFailed,
  PARTIAL_FAILURE: StepFailed,
  SKIPPED: StepSkipped,
  SUCCESSFUL: StepSuccessful,
  UNKNOWN: StepUnknown
};
