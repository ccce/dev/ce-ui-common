import { Stack, Tooltip, Typography } from "@mui/material";
import { theme } from "../../../style/Theme";
import {
  IconRunning,
  IconFailed,
  IconSuccessful,
  IconSkipped,
  IconUnknown
} from "../Icons";

/**
 * Styled device-responsive Stepper.
 * type Step {
    key: string;
    label: string;
    state: string;
  }
 * @param {Steps} step
 * @param {string} bgSize - in px deciding the size of background circle
 * @param {string} iconSize - xsmall | small | medium | large
 * @param {boolean} showToolTip - if true, step label is hidden and tooltip is enabled
 * @returns InlineStepper
 */

const IconWrapper = ({ sx = [], icon, color, bgSize, ...props }) => (
  <Stack
    flexDirection="row"
    justifyContent="center"
    alignItems="center"
    {...props}
    sx={[
      {
        width: bgSize || "20px",
        height: bgSize || "20px",
        borderRadius: "50%",
        background: color || "transparent",
        zIndex: 1
      },
      ...(Array.isArray(sx) ? sx : [sx])
    ]}
  >
    {icon}
  </Stack>
);

export const Step = ({
  icon,
  color,
  title,
  isError,
  bgSize,
  showToolTip,
  ...props
}) => (
  <Tooltip
    title={showToolTip ? title : ""}
    placement="bottom"
  >
    <Stack alignItems="center">
      <IconWrapper
        icon={icon}
        color={color}
        bgSize={bgSize}
        {...props}
      />
      {!showToolTip && (
        <Typography
          color={isError ? color : "black"}
          sx={{ marginTop: "8px" }}
        >
          {title}
        </Typography>
      )}
    </Stack>
  </Tooltip>
);

export const StepRunning = ({
  step,
  outlined,
  bgSize,
  iconSize,
  showToolTip
}) => (
  <Step
    title={step.label}
    color={outlined ? "transparent" : theme.palette.status.progress.main}
    bgSize={bgSize}
    showToolTip={showToolTip}
    icon={
      <IconRunning
        size={iconSize || "small"}
        color={theme.palette.essWhite.main}
      />
    }
  />
);

export const StepFailed = ({ step, bgSize, iconSize, showToolTip }) => (
  <Step
    title={step.label}
    color={theme.palette.status.fail.main}
    isError
    bgSize={bgSize}
    showToolTip={showToolTip}
    icon={
      <IconFailed
        size={iconSize || "small"}
        color={theme.palette.essWhite.main}
      />
    }
  />
);

export const StepSuccessful = ({ step, bgSize, iconSize, showToolTip }) => (
  <Step
    title={step.label}
    color={theme.palette.status.ok.main}
    bgSize={bgSize}
    showToolTip={showToolTip}
    icon={
      <IconSuccessful
        size={iconSize || "small"}
        color={theme.palette.essWhite.main}
      />
    }
  />
);

export const StepSkipped = ({ step, bgSize, iconSize, showToolTip }) => (
  <Step
    title={step.label}
    bgSize={bgSize}
    showToolTip={showToolTip}
    icon={<IconSkipped size={iconSize || "xsmall"} />}
    sx={{ border: `2px solid ${theme.palette.status.disabled.main}` }}
  />
);

export const StepQueued = ({ step, bgSize, showToolTip }) => (
  <Step
    title={step.label}
    bgSize={bgSize}
    showToolTip={showToolTip}
    sx={{ border: `2px dashed ${theme.palette.status.disabled.main}` }}
  />
);

export const StepUnknown = ({ step, bgSize, iconSize, showToolTip }) => (
  <Step
    title={step.label}
    bgSize={bgSize}
    showToolTip={showToolTip}
    icon={<IconUnknown size={iconSize || "xsmall"} />}
    sx={{ border: `2px solid ${theme.palette.status.disabled.main}` }}
  />
);
