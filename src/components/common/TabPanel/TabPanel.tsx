import { ReactNode, SyntheticEvent, useState } from "react";
import Tabs, { TabsProps } from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import { Stack } from "@mui/material";
import { useUniqueKeys } from "../../../hooks/useUniqueKeys";

interface TabContentProps {
  children: ReactNode;
  value: number;
  index: number;
}

const TabContent = ({ children, value, index, ...other }: TabContentProps) => {
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box>{children}</Box>}
    </div>
  );
};

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`
  };
}

export type Tab = {
  label: string;
  content: ReactNode;
};

interface TabPanelProps {
  tabs: Tab[];
  onTabChange?: (index: number, label?: string) => void;
  TabsProps?: TabsProps;
  renderTabs?: (tabs: ReactNode) => ReactNode;
  initialIndex?: number;
}
export const TabPanel = ({
  tabs,
  onTabChange,
  TabsProps,
  renderTabs,
  initialIndex
}: TabPanelProps) => {
  const [tabIndex, setTabIndex] = useState(
    initialIndex && initialIndex >= 0 ? initialIndex : 0
  );

  const tabsKeys = useUniqueKeys(tabs);
  const tabsContentKeys = useUniqueKeys(tabs);

  const handleChange = (_: SyntheticEvent, newValue: number) => {
    setTabIndex(newValue);
    if (onTabChange) {
      onTabChange(newValue, tabs.at(newValue)?.label); // index, label
    }
  };

  const renderTabElements = () => {
    return (
      <Tabs
        value={tabIndex}
        onChange={handleChange}
        aria-label="basic tabs example"
        {...TabsProps}
      >
        {tabsKeys &&
          tabs?.map((tab, i) => (
            <Tab
              key={tabsKeys[i]}
              label={tab.label}
              {...a11yProps(i)}
            />
          ))}
      </Tabs>
    );
  };

  const _renderTabs = () => {
    if (renderTabs) {
      return renderTabs(renderTabElements());
    }
    return renderTabElements();
  };

  return (
    <Stack
      sx={{ width: "100%" }}
      gap={1}
    >
      <Box>{_renderTabs()}</Box>
      {tabsContentKeys &&
        tabs?.map((tab, i) => (
          <TabContent
            key={tabsContentKeys[i]}
            value={tabIndex}
            index={i}
          >
            {tab.content}
          </TabContent>
        ))}
    </Stack>
  );
};
