/**
 * SearchBar component
 */
import { useEffect, useState } from "react";
import { CircularProgress, Grid, Fade, TextField, styled } from "@mui/material";
import CheckIcon from "@mui/icons-material/Check";
import { useEffectAfterMount } from "../../../hooks/useMountEffects";
import { useTypingTimer } from "../../../hooks/useTypingTimer";

/**
 * SearchBar
 * @param {object} props
 * @param {function} props.search
 * @param {string} props.query for restoring search text - optional
 * @param {boolean} props.loading return true or false
 * @param {string} props.placeholder placeholder for the search text - optional
 * @param {string} props.className containing css classname
 * @param {object} props.children child(ren) of search results
 */
export const SearchBar = styled(
  ({
    search,
    query = "",
    loading = false,
    placeholder = "Search",
    className,
    children
  }) => {
    const [value, setValue] = useState(query);
    const [queryString, stillTyping] = useTypingTimer({ init: value });
    const [showAnimation, setShowAnimation] = useState(false);
    const [checkMarkOn, setCheckMarkOn] = useState(false);

    useEffectAfterMount(() => {
      search(queryString);
    }, [search, queryString]);
    useEffectAfterMount(() => {
      if (!showAnimation && !loading) {
        setShowAnimation(true);
      }
    }, [showAnimation, loading, setShowAnimation]);

    const solidTime = 1250;
    const fadeTime = 1000;
    useEffect(() => {
      if (!loading) {
        setCheckMarkOn(true);
        const timeout = setTimeout(() => {
          setCheckMarkOn(false);
        }, solidTime);
        return () => clearTimeout(timeout);
      }
    }, [loading]);

    useEffect(() => {
      setValue(query);
    }, [query]);

    const Loading = (
      <Fade
        in
        className="CeuiSearchBar-LoadingAnimation"
      >
        <CircularProgress color="primary" />
      </Fade>
    );

    const Finished = (
      <Fade
        in={checkMarkOn}
        timeout={fadeTime}
        enter={false}
        className="CeuiSearchBar-FinishedAnimation"
        data-tour="searchbar-finished-animation"
      >
        <CheckIcon color="primary" />
      </Fade>
    );

    const LoadingAnimation = loading ? Loading : Finished;

    return (
      <Grid
        container
        spacing={2}
        className={`${className} CeuiSearchBar`}
      >
        <Grid
          item
          xs={12}
          md={12}
        >
          <TextField
            fullWidth
            variant="outlined"
            label={placeholder}
            value={value}
            onKeyUp={stillTyping}
            onChange={(newValue) => setValue(newValue.target.value)}
            className="CeuiSearchBar-Text"
            InputProps={{
              endAdornment: showAnimation && LoadingAnimation
            }}
          />
        </Grid>
        <Grid
          item
          xs={12}
          md={12}
        >
          {children}
        </Grid>
      </Grid>
    );
  }
)({});
