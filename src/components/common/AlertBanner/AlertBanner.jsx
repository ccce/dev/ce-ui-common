import { Alert, Button, Grid } from "@mui/material";
import { oneOf, string } from "prop-types";

export const AlertBanner = ({ type, message, link }) => {
  // If alarm type isn't specified, then assume 'info'; otherwise use the alarm type.
  const determineSeverity = (alarmType) => {
    return ["error", "warning", "success"].includes(alarmType?.toLowerCase())
      ? alarmType?.toLowerCase()
      : "info";
  };

  return (
    <Alert
      severity={determineSeverity(type)}
      action={
        link && (
          <Button
            target="_blank"
            href={link}
            color="inherit"
            size="small"
          >
            More Info
          </Button>
        )
      }
    >
      {message}
    </Alert>
  );
};
AlertBanner.propTypes = {
  type: oneOf([
    "info",
    "warning",
    "error",
    "success",
    "INFO",
    "WARNING",
    "ERROR",
    "SUCCESS"
  ]),
  message: string,
  link: string
};

export const AlertBannerList = ({ alerts = [] }) => {
  return (
    <Grid
      id="ioc-alerts"
      container
      spacing={1}
    >
      {alerts.map((alert) => (
        <Grid
          item
          xs={12}
          key={alert?.message || alert}
        >
          <AlertBanner {...alert} />
        </Grid>
      ))}
    </Grid>
  );
};
