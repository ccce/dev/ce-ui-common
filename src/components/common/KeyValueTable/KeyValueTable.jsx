/**
 * KeyValueTable
 * component for table containing key and value
 */
import { Box, Typography } from "@mui/material";
import { object, oneOf, bool, oneOfType, arrayOf, func } from "prop-types";
import { Table } from "../Table";

function makeOverline(key, value) {
  return (
    <Box sx={{ paddingY: 1 }}>
      <Typography variant="overline">{key}</Typography>
      <Typography
        component="span"
        sx={{ display: "block" }}
      >
        {value}
      </Typography>
    </Box>
  );
}

const defaultOptions = {
  minWidth: "50%",
  align: "left"
};

/**
 * KeyValueTable
 * @param {object} props
 * @param {object} props.obj object containing key value pairs
 * @param {string} props.variant callback fired when the component requests to be closed
 * @param {object} props.keyOptions object containing options and label for key column
 * @param {object} props.valueOptions object containing options and label for value column
 * @param {boolean} props.stripped flag to determine if the KeyValueTable rows should have alternating color or not
 */
export function KeyValueTable({
  obj,
  variant = "table",
  keyOptions = {},
  valueOptions = {},
  striped = false,
  sx,
  ...rest
}) {
  keyOptions = { ...defaultOptions, headerName: "Key", ...keyOptions };
  valueOptions = { ...defaultOptions, headerName: "Value", ...valueOptions };

  if (variant === "table") {
    const columns = [
      { field: "key", ...keyOptions },
      { field: "value", ...valueOptions }
    ];
    const rows = Object.entries(obj ?? {}).map(([key, value]) => ({
      id: key,
      key,
      value
    }));
    return (
      <Table
        {...{
          columns,
          rows,
          disableHover: true,
          striped: striped,
          sx,
          ...rest
        }}
      />
    );
  } else if (variant === "overline") {
    const columns = [{ field: "overlineColumn", ...valueOptions }];
    const rows = Object.entries(obj ?? {}).map(([key, value]) => ({
      id: key,
      overlineColumn: makeOverline(key, value)
    }));
    return (
      <Table
        {...{
          columns,
          rows,
          // Size height to match contents (otherwise it is fixed/static
          // and that doesn't work well for the stacked elements)
          getRowHeight: () => "auto",
          sx: [
            {
              border: 0,
              "& .MuiDataGrid-columnHeaders": {
                display: "none"
              }
            },
            // You cannot spread `sx` directly because `SxProps` (typeof sx) can be an array.
            ...(Array.isArray(sx) ? sx : [sx])
          ],
          SkeletonProps: {
            sx: {
              marginY: 2
            }
          },
          disableHover: true,
          striped: striped,
          ...rest
        }}
      />
    );
  }
}
KeyValueTable.propTypes = {
  obj: object,
  variant: oneOf(["table", "overline"]),
  keyOptions: object,
  valueOptions: object,
  striped: bool,
  sx: oneOfType([arrayOf(oneOfType([func, object, bool])), func, object])
};
