import {
  RotateRightOutlined,
  ErrorOutline,
  CheckCircleOutline
} from "@mui/icons-material";
import FastForwardIcon from "@mui/icons-material/FastForward";
import QuestionMarkIcon from "@mui/icons-material/QuestionMark";
import { theme } from "../../../style/Theme";

export const IconRunning = ({ size, color }) => (
  <RotateRightOutlined
    fontSize={size || "inherit"}
    style={{ color: color || theme.palette.status.progress.main }}
  />
);

export const IconFailed = ({ size, color }) => (
  <ErrorOutline
    fontSize={size || "inherit"}
    style={{ color: color || theme.palette.status.fail.main }}
  />
);

export const IconSuccessful = ({ size, color }) => (
  <CheckCircleOutline
    fontSize={size || "inherit"}
    style={{ color: color || theme.palette.status.ok.main }}
  />
);

export const IconSkipped = ({ size, color }) => (
  <FastForwardIcon
    fontSize={size || "inherit"}
    style={{ color: color || theme.palette.status.disabled.main }}
  />
);

export const IconUnknown = ({ size, color }) => (
  <QuestionMarkIcon
    fontSize={size || "inherit"}
    style={{ color: color || theme.palette.status.disabled.main }}
  />
);
