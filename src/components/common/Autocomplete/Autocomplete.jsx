import {
  TextField,
  Autocomplete as MuiAutocomplete,
  styled,
  CircularProgress
} from "@mui/material";
import { bool, shape, object } from "prop-types";
import { theme } from "../../../style/Theme";

const propTypes = {
  loading: bool,
  autocompleteProps: object,
  /* eslint-disable react/forbid-foreign-prop-types */
  textFieldProps: shape(TextField.propTypes)
};

/**
 * Wraps the MUI Autocomplete with default loading behaviors in async use cases.
 */
const Autocomplete = styled(
  ({ loading, autocompleteProps, textFieldProps }) => {
    return (
      <MuiAutocomplete
        autoHighlight
        loading={loading}
        clearOnBlur={false}
        autoSelect
        renderInput={(params) => (
          <TextField
            {...params}
            {...textFieldProps}
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <>
                  {loading ? (
                    <CircularProgress
                      color="inherit"
                      size={20}
                    />
                  ) : null}
                  {params.InputProps.endAdornment}
                </>
              )
            }}
          />
        )}
        {...autocompleteProps}
        // MUI's component will display options even though the component
        // may be loading; this enforces that options will only be visible
        // if the component is finished loading.
        options={loading ? [] : autocompleteProps.options}
      />
    );
  }
)(theme);

Autocomplete.propTypes = propTypes;

export { Autocomplete };
