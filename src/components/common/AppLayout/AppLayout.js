/**
 * Styled MUI layout components
 */
import { Box, styled } from "@mui/material";

export const AppBox = styled(Box)({
  height: "100%",
  display: "flex",
  flexFlow: "column",
  padding: 0
});

export const HeaderBox = styled(Box)({
  display: "flex",
  flexFlow: "column",
  flex: "0 1 auto",
  padding: 0
});

export const BodyBox = styled(Box)({
  display: "flex",
  flexFlow: "row",
  flex: "1 1 auto",
  padding: 0
});

export const ContentBox = styled(Box)({
  display: "flex",
  flexFlow: "column",
  flex: "1 1 auto",
  padding: 0
});
