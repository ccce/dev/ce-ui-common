import EventIcon from "@mui/icons-material/Event";
import { Box, Tooltip } from "@mui/material";
import { bool, instanceOf } from "prop-types";
import { Duration } from "./Duration";
import { LabeledIcon } from "../LabeledIcon";
import { formatDateAndTime, formatRelativeTime } from "../../../lib/Time-Utils";

const propTypes = {
  /** Start time */
  startTime: instanceOf(Date),
  /** Created time */
  createdAt: instanceOf(Date),
  /** Date job is finished */
  finishedAt: instanceOf(Date),
  /** If to render the inner contents */
  textOnly: bool
};

const CreateStartInfo = ({ createOrStartDate, initString }) => {
  const formattedRelativeCreateOrStartDate =
    formatRelativeTime(createOrStartDate);
  const formattedCreateOrStartDate = formatDateAndTime(createOrStartDate);

  return (
    <Tooltip
      title={`${initString} ${formattedCreateOrStartDate}`}
      aria-label={`${initString} ${formattedRelativeCreateOrStartDate} at ${formattedCreateOrStartDate}`}
    >
      <LabeledIcon
        label={`${initString} ${formattedRelativeCreateOrStartDate}`}
        LabelProps={{ variant: "body2" }}
        labelPosition="right"
        Icon={EventIcon}
        IconProps={{ fontSize: "small" }}
      />
    </Tooltip>
  );
};

export const StartAndDuration = ({
  startTime,
  createdAt,
  finishedAt,
  textOnly
}) => {
  const createOrStartDate = startTime ?? createdAt;
  const initString = startTime ? "Started" : "Created";
  return (
    <Box>
      <CreateStartInfo
        createOrStartDate={createOrStartDate}
        initString={initString}
      />
      {finishedAt ? (
        <Duration
          createOrStartDate={createOrStartDate}
          finishedAt={finishedAt}
          textOnly={textOnly}
        />
      ) : null}
    </Box>
  );
};

StartAndDuration.propTypes = propTypes;
