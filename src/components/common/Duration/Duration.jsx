import AccessTimeIcon from "@mui/icons-material/AccessTime";
import { Tooltip, Typography } from "@mui/material";
import { bool, instanceOf } from "prop-types";
import { LabeledIcon } from "../LabeledIcon";
import { formatDateAndTime, formattedDuration } from "../../../lib/Time-Utils";

const propTypes = {
  /** Date that is start or creation date */
  createOrStartDate: instanceOf(Date),
  /** Date job is finished */
  finishedAt: instanceOf(Date),
  /** If to render the inner contents */
  textOnly: bool
};

export const Duration = ({ createOrStartDate, finishedAt, textOnly }) => {
  const duration = formattedDuration({
    finishDate: new Date(finishedAt),
    startDate: new Date(createOrStartDate)
  });

  const formattedFinishedAt = formatDateAndTime(finishedAt);

  return (
    <Tooltip
      title={`Finished ${formattedFinishedAt}`}
      aria-label={`Finished ${formattedFinishedAt}, after ${duration}`}
    >
      {textOnly ? (
        <Typography>{`${duration}`}</Typography>
      ) : (
        <LabeledIcon
          label={`${duration}`}
          LabelProps={{ variant: "body2" }}
          labelPosition="right"
          Icon={AccessTimeIcon}
          IconProps={{ fontSize: "small" }}
        />
      )}
    </Tooltip>
  );
};

Duration.prototypes = propTypes;
