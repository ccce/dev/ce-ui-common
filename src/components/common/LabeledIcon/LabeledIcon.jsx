import { forwardRef } from "react";
import { node, oneOfType, string, oneOf, object, func } from "prop-types";
import { Stack, Tooltip, Typography } from "@mui/material";

/**
 * Component that renders an icon with a label.
 *
 * @param {object | func} Icon icon reference (provided as e.g. TheIcon, or () => <TheIcon />, not as <TheIcon />)
 * @param {object} IconProps additional props to pass to the Icon if it is provided as object eg TheIcon. Accessibility props are already provided.
 * @param {string} label text or Component to render as the label for the icon
 * @param {object} LabelProps additional props to provide to the label when rendered as a Typography component
 * (not applicable when positioned as "tooltip" or "none").
 * @param {string} labelPosition describes position of the label. "tooltip" provides the label as a tooltip,
 * "left" and "right" positions the label to the left or right respectively, and "none" provides no visible label
 * (but still includes a11y attributes so that it is visible to screen readers)
 */
export const LabeledIcon = forwardRef(function LabeledIcon(props, ref) {
  const {
    className,
    Icon,
    IconProps,
    label,
    LabelProps,
    labelPosition = "tooltip",
    ...rest
  } = props;

  if (labelPosition === "tooltip") {
    return (
      <Tooltip
        title={label}
        className={className}
      >
        <Icon
          titleAccess={label}
          {...IconProps}
        />
      </Tooltip>
    );
  }
  if (labelPosition === "none") {
    return (
      <Icon
        titleAccess={label}
        {...IconProps}
      />
    );
  }

  const renderedLabel = <Typography {...LabelProps}>{label}</Typography>;
  return (
    <Stack
      flexDirection="row"
      gap={0.5}
      alignItems="center"
      className={className}
      ref={ref}
      {...rest}
    >
      {labelPosition === "left" ? renderedLabel : null}
      <Icon {...IconProps} />
      {labelPosition === "right" ? renderedLabel : null}
    </Stack>
  );
});
LabeledIcon.propTypes = {
  Icon: oneOfType([object, func]),
  IconProps: object,
  label: oneOfType([string, node]),
  LabelProps: object,
  labelPosition: oneOf(["tooltip", "right", "left", "none"])
};
