import { string } from "prop-types";
import { Alert } from "@mui/material";

export const MaintenanceModeBanner = ({ message }) => {
  return (
    <>
      <Alert
        variant="filled"
        severity="warning"
        sx={{ width: "100%" }}
      >
        {message}
      </Alert>
    </>
  );
};

MaintenanceModeBanner.propTypes = {
  message: string
};
