import { composeStories } from "@storybook/react";
import * as stories from "../../../stories/common/SimpleAccordion/SimpleAccordion.stories";

const { DefaultExpanded, CustomSummary } = composeStories(stories);

describe("Default heading level", () => {
  it("should have default H3 element", () => {
    cy.mount(<DefaultExpanded />);
    cy.findByRole("heading", { name: "Sample accordion", level: 3 }).should(
      "exist"
    );
  });
});

describe("Override heading level", () => {
  it("should override default H3 element", () => {
    cy.mount(<CustomSummary />);
    cy.findByRole("heading", {
      name: "With a custom summary component",
      level: 2
    }).should("exist");
  });
});
