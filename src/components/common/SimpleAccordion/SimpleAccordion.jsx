/**
 * SimpleAccordion
 * component for table containing key and value
 */
import { number, string, element, object, oneOfType, array } from "prop-types";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Typography,
  styled
} from "@mui/material";
import { ExpandMore } from "@mui/icons-material";

const propTypes = {
  elevation: number,
  summary: oneOfType([string, element]),
  className: string,
  children: oneOfType([string, element, array, object])
};

/**
 * SimpleAccordion
 * @param {object} props
 * @param {number} props.elevation MUI elevation of accordion
 * @param {string} props.summary title
 * @param {string} props.className containing css classname
 * @param {string} props.children string with accordion content
 */
export const SimpleAccordion = styled(
  ({ summary, children, className, elevation = 0, ...rest }) => {
    if (typeof summary === "string") {
      summary = <Typography variant="h3">{summary}</Typography>;
    }

    if (typeof children === "string") {
      children = <Typography>{children}</Typography>;
    }

    return (
      <Accordion
        elevation={elevation}
        className={`${className} CeuiSimpleAccordion`}
        {...rest}
      >
        <AccordionSummary
          expandIcon={<ExpandMore />}
          className="CeuiSimpleAccordion-Summary"
        >
          {summary}
        </AccordionSummary>
        <AccordionDetails className="CeuiSimpleAccordion-Details">
          {children}
        </AccordionDetails>
      </Accordion>
    );
  }
)(({ theme, elevation }) => {
  return {
    ...(!elevation && {
      border: `1px solid ${theme.palette.grey[300]}`,
      borderRadius: theme.spacing(0.5),
      "&.Mui-expanded": {
        ".CeuiSimpleAccordion-Summary": {
          padding: 0
        },
        borderColor: "transparent"
      },
      "&:before": {
        display: "none"
      },
      ".CeuiSimpleAccordion-Details": {
        padding: 0
      }
    }),
    marginTop: theme.spacing(2)
  };
});

SimpleAccordion.propTypes = propTypes;
