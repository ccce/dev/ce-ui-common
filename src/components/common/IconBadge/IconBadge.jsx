import { Grid, Typography, Box, styled } from "@mui/material";
import { theme } from "../../../style/Theme";

const IconColumn = ({ children, ...props }) => {
  return (
    <Box
      display="flex"
      {...props}
    >
      {children}
    </Box>
  );
};

export const IconBadge = styled(({ icon, secondaryIcon, title, subtitle }) => {
  return (
    <Grid
      container
      alignItems="center"
      direction="row"
    >
      <Grid
        item
        xs={3}
        sm={2}
        md={1}
      >
        <Grid
          container
          justify="center"
          direction="column"
          alignItems="center"
        >
          <IconColumn>{icon}</IconColumn>
          <IconColumn
            sx={{
              opacity: 0.6,
              transform: "scale(0.80)"
            }}
          >
            {secondaryIcon}
          </IconColumn>
        </Grid>
      </Grid>
      <Grid
        item
        xs={9}
        sm={10}
        md={11}
      >
        <Grid
          container
          justifyContent="flex-start"
        >
          <Grid
            item
            xs={12}
            zeroMinWidth
          >
            <Typography
              noWrap
              variant="subtitle1"
              component="p"
              color="textPrimary"
            >
              {title}
            </Typography>
          </Grid>
          <Grid
            item
            xs={12}
            zeroMinWidth
          >
            <Typography
              noWrap
              component="p"
              variant="subtitle2"
            >
              {subtitle}
            </Typography>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
})(theme);
