/**
 * AccessControl
 * component checking user access & user role(s)
 */
import { useUserContext } from "../../contexts/User";

/**
 * Checks if a given set of user roles permits an action.
 * If allowedRoles is empty or undefined, then action is permitted.
 * Otherwise, at least one of the user roles must match the allowed roles.
 *
 * @param {string[]} userRoles Roles of the user
 * @param {string[]} allowedRoles Roles that allow access for this user
 * @returns {boolean} true if action is permitted
 */
const checkPermissions = (userRoles, allowedRoles) => {
  if (allowedRoles?.length === 0) {
    return true;
  }

  return userRoles?.some((role) => allowedRoles?.includes(role));
};

/**
 * Defines access for a specific user with a specific set of roles, for example to only
 * permit an user with admin roles to perform admin actions on only their own resources.
 *
 * If allowedUsersWithRoles is empty or undefined, then action is forbidden.
 * Otherwise, permit action if any definition in allowedUsersWithRoles meets both criteria:
 *   - username matches user
 *   - role matches at least one userRole
 *
 * @param {object} user User to be evaluated
 * @param {string} user.loginName Login name of the user
 * @param {string[]} userRoles Roles of the user
 * @param {object[]} allowedUsersWithRoles Conditions the user will be evaluated against
 * @param {string} allowedUsersWithRoles[].user Login name
 * @param {string} allowedUsersWithRoles[].role Role name
 * @returns {boolean} true if action is permitted
 */
const checkUser = (user, userRoles, allowedUsersWithRoles) => {
  if (!allowedUsersWithRoles || allowedUsersWithRoles?.length === 0) {
    return false;
  }

  return allowedUsersWithRoles.find(
    (userWithRole) =>
      userWithRole?.user === user?.loginName &&
      userRoles?.includes(userWithRole.role)
  );
};

/**
 * Permits an action for an user if either is true:
 *   - allowedRoles is empty, or userRoles match at least one of those roles, OR
 *   - if any definition in allowedUsersWithRoles meets both criteria:
 *     - username matches user, AND
 *     - role matches at least one userRole
 *
 * @param {object} user User to be evaluated
 * @param {string} user.loginName Login name of the user
 * @param {string[]} userRoles Roles of the user
 * @param {string[]} allowedRoles Roles that permit access to the user
 * @param {object[]} allowedUsersWithRoles Conditions the user will be evaluated against
 * @param {string} allowedUsersWithRoles[].user Login name
 * @param {string} allowedUsersWithRoles[].role Role name
 * @returns {boolean} true if action is permitted
 */
export const isPermitted = ({
  user,
  userRoles,
  allowedRoles,
  allowedUsersWithRoles
}) => {
  return (
    checkPermissions(userRoles, allowedRoles) ||
    checkUser(user, userRoles, allowedUsersWithRoles)
  );
};

export const useIsCurrentUserPermitted = ({
  allowedRoles,
  allowedUsersWithRoles
}) => {
  const { user, userRoles } = useUserContext();

  return isPermitted({ user, userRoles, allowedRoles, allowedUsersWithRoles });
};

export function AccessControl({
  allowedRoles,
  allowedUsersWithRoles,
  children,
  renderNoAccess
}) {
  const permitted = useIsCurrentUserPermitted({
    allowedRoles,
    allowedUsersWithRoles
  });
  if (permitted) {
    return children;
  }

  return renderNoAccess();
}
