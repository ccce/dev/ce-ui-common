/**
 * Login
 * component for returning username & password
 */
import { useState } from "react";
import {
  TextField,
  Button,
  LinearProgress,
  Alert,
  Grid,
  styled
} from "@mui/material";
import { Dialog } from "../common/Dialog";

/**
 * Login form
 * @param {object} props
 * @param {function} props.login function returning username & password
 * @param {string} props.error containing error message
 * @param {string} props.className containing css classname
 */
export const LoginForm = styled(({ login, error, loading, className }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  return (
    <form className={`${className} CeuiLoginForm`}>
      <Grid
        container
        spacing={1}
      >
        <Grid
          item
          xs={12}
          md={12}
        >
          <TextField
            label="Username"
            variant="outlined"
            onChange={(e) => {
              setUsername(e.target.value);
            }}
            fullWidth
            autoFocus
            required
            className="CeuiLoginForm-UserName"
          />
        </Grid>
        <Grid
          item
          xs={12}
          md={12}
        >
          <TextField
            label="Password"
            variant="outlined"
            type="password"
            onChange={(e) => {
              setPassword(e.target.value);
            }}
            fullWidth
            required
            className="CeuiLoginForm-Password"
          />
        </Grid>
        <Grid
          item
          xs={12}
          md={12}
        >
          {error && (
            <Alert
              data-test="alert"
              severity="error"
              className="CeuiLoginForm-Error"
            >
              {error}
            </Alert>
          )}
        </Grid>
        <Grid
          item
          xs={12}
          md={12}
        >
          <Button
            data-test="submitButton"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(e) => {
              e.preventDefault();
              login(username, password);
            }}
            type="submit"
            className="CeuiLoginForm-LoginButton"
          >
            Login
          </Button>
        </Grid>
        <Grid
          item
          xs={12}
          md={12}
        >
          {loading && (
            <LinearProgress
              data-test="progressbar"
              className="CeuiLoginForm-LoadingIndicator"
            />
          )}
        </Grid>
      </Grid>
    </form>
  );
})({});

/**
 * Dialog with login form
 * @param {object} props
 * @param {function} props.login function returning username & password
 * @param {string} props.error containing error message
 * @param {string} props.className containing css classname
 * @param {boolean} props.open return true or false
 * @param {function} props.handleClose callback fired when the component requests to be closed
 */
export const LoginDialog = styled(
  ({ login, open, error, loading, handleClose, className }) => {
    return (
      <Dialog
        DialogProps={{
          fullWidth: true,
          open: open
        }}
        onClose={handleClose}
        className={`${className} CeuiLoginDialog`}
        content={
          <LoginForm
            login={login}
            error={error}
            loading={loading}
          />
        }
      />
    );
  }
)({});
