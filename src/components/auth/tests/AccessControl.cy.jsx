import { composeStories } from "@storybook/react";
import * as stories from "../../../stories/auth/AccessControl.stories";

const { Default } = composeStories(stories);

const testUser = {
  loginName: "test user"
};

const mountTestComponent = ({
  testUser,
  testRoles,
  allowedRoles,
  allowedUsersWithRoles
}) => {
  cy.mount(
    <Default
      {...{ testUser, testRoles, allowedRoles, allowedUsersWithRoles }}
    />
  );
};

const isAllowed = () => {
  cy.findByText("Result: Allowed!").should("exist");
};

const isForbidden = () => {
  cy.findByText("Result: Forbidden!").should("exist");
};

describe("AccessControl.spec.js", () => {
  context("Access Control", () => {
    it("Does not allow anonymous users", () => {
      mountTestComponent({
        testUser: undefined
      });
      isForbidden();
    });
    it("Does not allow any users", () => {
      mountTestComponent({
        testUser,
        allowedRoles: null
      });
      isForbidden();
    });

    it("Allows authenticated user", () => {
      mountTestComponent({
        testUser,
        allowedRoles: []
      });
      isAllowed();
    });

    it("Allows user matching available roles", () => {
      mountTestComponent({
        testUser,
        testRoles: ["banana"],
        allowedRoles: ["apple", "banana"]
      });
      isAllowed();
    });

    it("Forbids user missing required roles", () => {
      mountTestComponent({
        testUser,
        testRoles: ["blueberry"],
        allowedRoles: ["apple", "banana"]
      });
      isForbidden();
    });

    it("Forbids specific user without matching role", () => {
      mountTestComponent({
        testUser,
        allowedUsersWithRoles: [{ user: testUser.loginName }]
      });
      isForbidden();
    });
    it("Allows specific user with role", () => {
      mountTestComponent({
        testUser,
        testRoles: ["lion"],
        allowedUsersWithRoles: [
          { user: testUser.loginName, role: "parrot" },
          { user: testUser.loginName, role: "rhino" },
          { user: testUser.loginName, role: "lion" }
        ]
      });
      isAllowed();
    });
    it("Forbids user not matching username", () => {
      mountTestComponent({
        testUser,
        allowedUsersWithRoles: [{ user: "johnwick" }]
      });
      isForbidden();
    });
  });
});
