/**
 * Login component cypress testing
 */
import { useState } from "react";
import { Button, Box } from "@mui/material";
import { LoginForm, LoginDialog } from "../Login";

// Dummy login username & password for passing test
const USERNAME = "username";
const PASSWORD = "password";

function LoginFormTemplate({ dialog }) {
  const [loginError, setLoginError] = useState("");
  const [loginLoading, setLoginLoading] = useState(false);
  const [open, setOpen] = useState(false);

  const login = (username, password) => {
    setLoginLoading(true);
    if (username !== USERNAME || password !== PASSWORD) {
      setLoginError("Bad username/password");
      setLoginLoading(false);
    }
  };

  if (dialog) {
    return (
      <Box>
        <Button
          data-test="launchButton"
          onClick={() => setOpen(!open)}
        >
          Login
        </Button>
        <LoginDialog
          className="cypressTest"
          login={login}
          open={open}
          error={loginError}
          loading={loginLoading}
          handleClose={() => setOpen(!open)}
        />
      </Box>
    );
  } else {
    return (
      <LoginForm
        className="cypressTest"
        login={login}
        error={loginError}
        loading={loginLoading}
      />
    );
  }
}

describe("Login.spec.js", () => {
  context("Login From", () => {
    beforeEach(() => {
      cy.mount(<LoginFormTemplate />);
    });

    it("Login should succeed", () => {
      cy.findByRole("textbox", { name: /username/i }).type("username");
      cy.findByLabelText(/password/i).type("password");
      cy.getByData("submitButton").click();
      cy.getByData("progressbar").should("exist");
    });

    it("Login should fail", () => {
      cy.findByRole("textbox", { name: /username/i }).type("usr");
      cy.findByLabelText(/password/i).type("pwd");
      cy.getByData("submitButton").click();
      cy.getByData("alert").should("exist");
    });
  });

  context("Login with Dialog", () => {
    beforeEach(() => {
      cy.mount(<LoginFormTemplate dialog />);
    });

    it("Launch login dialog and login", () => {
      cy.getByData("launchButton").click();
      cy.findByRole("textbox", { name: /username/i }).type("username");
      cy.findByLabelText(/password/i).type("password");
      cy.get("Button[type=submit]").click();
      cy.getByData("progressbar").should("exist");
    });
  });
});
