import { createContext, useContext } from "react";

interface GlobalAppContext {
  title: string;
  setTitle: (title: string) => void;
}

export const GlobalAppBarContext = createContext<GlobalAppContext>({
  title: "",
  setTitle: () => {
    console.info("default GlobalAppBarContext.setTitle does nothing.");
  }
});

export const useGlobalAppBarContext = () => useContext(GlobalAppBarContext);
