import { createContext, useContext } from "react";

export const userContext = createContext({
  user: null,
  userRoles: [],
  login: null,
  logout: null
});

export const useUserContext = () => {
  const { user, userRoles, login, logout } = useContext(userContext);
  return { user, userRoles, login, logout };
};
