import "@fontsource/titillium-web/200.css";
import "@fontsource/titillium-web/300.css";
import "@fontsource/titillium-web/400.css";
import "@fontsource/titillium-web/600.css";
import "@fontsource/titillium-web/900.css";
import { createTheme, responsiveFontSizes } from "@mui/material";
import { yellow, grey } from "@mui/material/colors";

export let theme = createTheme({});

export const ess = {
  cyan: "#0099dc",
  black: "#000000",
  white: "#ffffff",
  purple: "#821482",
  orange: "#ff7d00",
  forest: "#006646",
  grass: "#99be00",
  navy: "#003366",
  lightgrey: "#e6e6e6"
};

declare module "@mui/material/styles" {
  interface Palette {
    essCyan: Palette["primary"];
    essBlack: Palette["primary"];
    essWhite: Palette["primary"];
    essPurple: Palette["primary"];
    essOrange: Palette["primary"];
    essForest: Palette["primary"];
    essGrass: Palette["primary"];
    essNavy: Palette["primary"];
    environment: {
      production: Palette["primary"];
      development: Palette["primary"];
    };
  }

  interface PaletteOptions {
    essCyan: Palette["primary"];
    essBlack: Palette["primary"];
    essWhite: Palette["primary"];
    essPurple: Palette["primary"];
    essOrange: Palette["primary"];
    essForest: Palette["primary"];
    essGrass: Palette["primary"];
    essNavy: Palette["primary"];
    environment: {
      production: Palette["primary"];
      development: Palette["primary"];
    };
  }
}

theme = createTheme(theme, {
  palette: {
    essCyan: theme.palette.augmentColor({
      color: {
        main: ess.cyan
      },
      name: "essCyan"
    }),
    essBlack: theme.palette.augmentColor({
      color: {
        main: ess.black
      },
      name: "essBlack"
    }),
    essWhite: theme.palette.augmentColor({
      color: {
        main: ess.white
      },
      name: "essWhite"
    }),
    essPurple: theme.palette.augmentColor({
      color: {
        main: ess.purple
      },
      name: "essPurple"
    }),
    essOrange: theme.palette.augmentColor({
      color: {
        main: ess.orange
      },
      name: "essOrange"
    }),
    essForest: theme.palette.augmentColor({
      color: {
        main: ess.forest
      },
      name: "essForest"
    }),
    essGrass: theme.palette.augmentColor({
      color: {
        main: ess.grass
      },
      name: "essGrass"
    }),
    essNavy: theme.palette.augmentColor({
      color: {
        main: ess.navy
      },
      name: "essNavy"
    })
  }
});

theme = createTheme(theme, {
  palette: {
    primary: {
      main: theme?.palette?.essCyan?.main,
      lightText: theme.palette.essWhite,
      mediumText: grey[800]
    },
    primaryContrastText: {
      main: theme.palette.primary.contrastText
    },
    secondary: theme.palette.augmentColor({
      color: {
        main: theme.palette.essNavy.main
      },
      name: "secondary"
    }),
    environment: {
      production: theme.palette.augmentColor({
        color: {
          main: theme.palette.essCyan.main
        },
        name: "production"
      }),
      development: theme.palette.augmentColor({
        color: {
          main: theme.palette.essOrange.main
        },
        name: "development"
      })
    },
    status: {
      ok: theme.palette.augmentColor({
        color: {
          main: theme.palette.success.main
        },
        name: "ok"
      }),
      progress: theme.palette.augmentColor({
        color: {
          main: theme.palette.warning.light
        },
        name: "progress"
      }),
      fail: theme.palette.augmentColor({
        color: {
          main: theme.palette.error.main
        },
        name: "fail"
      }),
      disabled: theme.palette.augmentColor({
        color: {
          main: theme.palette.grey[500]
        },
        name: "disabled"
      })
    },
    warning: theme.palette.augmentColor({
      color: {
        main: yellow[500],
        light: yellow[400],
        dark: yellow[600]
      },
      name: "warning"
    })
  }
});

const titilliumWeight = {
  thin: 200,
  light: 300,
  regular: 400,
  semibold: 600,
  bold: 900
};

const fontFamily = ["Titillium Web", "Segoe UI", "sans-serif"].join(",");

theme = createTheme(theme, {
  typography: {
    h1: {
      fontFamily,
      fontWeight: titilliumWeight.regular,
      fontSize: theme.typography.h3.fontSize
    },
    h2: {
      fontFamily,
      fontWeight: titilliumWeight.regular,
      fontSize: theme.typography.h4.fontSize
    },
    h3: {
      fontFamily,
      fontWeight: titilliumWeight.semibold,
      fontSize: theme.typography.h6.fontSize
    },
    h4: undefined,
    h5: undefined,
    h6: undefined,
    subtitle1: { fontFamily },
    subtitle2: {
      fontFamily,
      fontWeight: titilliumWeight.light
    },
    body1: { fontFamily },
    body2: {
      fontFamily,
      fontWeight: titilliumWeight.light
    },
    button: { fontFamily },
    caption: {
      fontFamily,
      fontWeight: titilliumWeight.light,
      fontStyle: "italic"
    },
    overline: { fontFamily },
    code: {
      fontFamily: "monospace"
    }
  }
});

// Define font weights
theme = createTheme(theme, {
  typography: {
    fontWeightLight: titilliumWeight.thin,
    fontWeightRegular: titilliumWeight.light,
    fontWeightMedium: titilliumWeight.regular,
    fontWeightBold: titilliumWeight.semibold
  }
});

// Compose responsive font sizes
theme = responsiveFontSizes(theme);

// Component overrides
theme = createTheme(theme, {
  components: {
    MuiListItemButton: {
      styleOverrides: {
        root: {
          "&.Mui-selected, &.Mui-selected:hover": {
            backgroundColor: "#cccccc"
          }
        }
      }
    },
    MuiLink: {
      styleOverrides: {
        root: {
          textDecoration: "none",
          ":hover": {
            textDecoration: "underline"
          }
        }
      }
    }
  }
});
