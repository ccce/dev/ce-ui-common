interface withPaginationProps {
  page: number;
  limit: number;
  data: unknown[];
}

export const withPagination = ({ page, limit, data }: withPaginationProps) => {
  const startIndex = page * limit;
  const pagedData = data.slice(startIndex, startIndex + limit);

  return {
    limit,
    listSize: pagedData.length,
    pageNumber: page,
    totalCount: data.length,
    data: pagedData,
    DEVELOPER_NOTE: "THIS IS MOCKED PAGING!"
  };
};
