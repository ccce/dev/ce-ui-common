import { createContext } from "react";

export const apiContext = createContext(null);

export const severityMap = {
  unknown: "warn",
  error: "error"
};

export const columns = [
  { field: "name", headerName: "Host" },
  { field: "description", headerName: "Description" },
  {
    field: "network",
    headerName: "Network"
  },
  { field: "scope", headerName: "Network Scope" }
];
