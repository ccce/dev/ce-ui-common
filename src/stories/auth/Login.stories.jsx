import { useState } from "react";
import { Card, CardContent, Grid, Box, Button } from "@mui/material";
import { LoginForm, LoginDialog } from "../../components/auth/Login";

export default {
  title: "Auth/Login"
};

const loginError = "";

export const BasicLoginForm = () => {
  const login = (username, password) => {
    alert(`User log in: ${username}, ${password}`);
  };

  return (
    <Grid
      container
      spacing={0}
      alignItems="center"
      justify="center"
    >
      <Grid
        item
        xs={8}
        md={5}
        xl={3}
      >
        <Card>
          <CardContent>
            <LoginForm
              login={login}
              error={loginError}
            />
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export const BasicLoginDialog = () => {
  const [open, setOpen] = useState(false);
  const login = (username, password) => {
    alert(`User log in: ${username}, ${password}`);
  };
  const toggleOpen = () => setOpen(!open);

  return (
    <div>
      <Box sx={{ width: "30%" }}>
        <Button
          fullWidth
          variant="contained"
          color="primary"
          onClick={toggleOpen}
        >
          Login
        </Button>
      </Box>
      <LoginDialog
        login={login}
        open={open}
        error={loginError}
        handleClose={toggleOpen}
        idUsername="dialog-username"
        idPassword="dialog-password"
      />
    </div>
  );
};
