import { Box, Paper, Typography } from "@mui/material";
import { useContext, useEffect, useState, useMemo } from "react";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import CancelIcon from "@mui/icons-material/Cancel";
import { AccessControl } from "../../components/auth/AccessControl";
import { userContext } from "../../contexts/User";

export default {
  title: "Auth/AccessControl"
};

const testAuthContext = userContext;

const TestAuthContextProvider = ({ children }) => {
  const [user, setUser] = useState();
  const [userRoles, setUserRoles] = useState([]);

  const value = useMemo(
    () => ({
      user,
      setUser,
      userRoles,
      setUserRoles
    }),
    [user, setUser, userRoles, setUserRoles]
  );

  return (
    <testAuthContext.Provider value={value}>
      {children}
    </testAuthContext.Provider>
  );
};

const Harness = ({ children }) => {
  return <TestAuthContextProvider>{children}</TestAuthContextProvider>;
};

const TestComponent = ({
  testUser,
  testRoles,
  allowedRoles,
  allowedUsersWithRoles
}) => {
  const { user, userRoles, setUser, setUserRoles } =
    useContext(testAuthContext);

  useEffect(() => {
    setUser(testUser);
    setUserRoles(testRoles);
  }, [setUser, setUserRoles, testUser, testRoles]);

  return (
    <Paper>
      <Typography>testUser: {user?.loginName}</Typography>
      <Typography>testRoles: {userRoles?.join(", ")}</Typography>
      <Typography>allowedRoles: {allowedRoles?.join(", ")}</Typography>
      <Typography>
        allowedUsersWithRoles:{" "}
        {allowedUsersWithRoles
          ?.map((it) => `{user: ${it?.user}, role: ${it?.role}}`)
          .join(", ")}
      </Typography>
      <Box marginTop={2}>
        <AccessControl
          allowedRoles={allowedRoles}
          allowedUsersWithRoles={allowedUsersWithRoles}
          renderNoAccess={() => (
            <Typography
              display="flex"
              alignContent="center"
            >
              Result: Forbidden! <CancelIcon color="error" />
            </Typography>
          )}
        >
          <Typography
            display="flex"
            alignContent="center"
          >
            Result: Allowed! <CheckCircleIcon color="success" />
          </Typography>
        </AccessControl>
      </Box>
    </Paper>
  );
};

const Template = (args) => {
  return (
    <Harness>
      <TestComponent {...args} />
    </Harness>
  );
};

export const Default = (args) => <Template {...args} />;
Default.args = {
  testUser: { loginName: "test user" },
  testRoles: ["admin", "user"],
  allowedRoles: ["user"],
  allowedUsersWithRoles: [{ user: "test user", role: "user" }]
};
