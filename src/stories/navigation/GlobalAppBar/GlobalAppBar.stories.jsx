import { useState } from "react";
import { Home, AddBox } from "@mui/icons-material";
import { Button, IconButton as MuiIconButton, Tooltip } from "@mui/material";
import { action } from "@storybook/addon-actions";
import BugReportIcon from "@mui/icons-material/BugReport";
import HelpCenterIcon from "@mui/icons-material/HelpCenter";
import { GlobalAppBar } from "../../../components/navigation/GlobalAppBar";
import { IconMenuButton } from "../../../components/common/IconMenuButton";
import { LoremSuperLong } from "../../utils/common-components";
import { sbDisabledArg } from "../../utils/common-args";
import { MaintenanceModeBanner } from "../../../components/common/MaintenanceModeBanner/MaintenanceModeBanner";
import { BodyBox } from "../../../components/common/AppLayout";
import { theme } from "../../../style/Theme";
import { Environment } from "../../../components/navigation/Environment";

export default {
  title: "Navigation/GlobalAppBar",
  parameters: {
    layout: "fullscreen"
  },
  argTypes: {
    defaultHomeButton: sbDisabledArg,
    defaultActionButton: sbDisabledArg,
    defaultIconMenuButton: sbDisabledArg,
    defaultHelpButton: sbDisabledArg,
    environment: {
      options: [
        Environment.PROD,
        Environment.TEST,
        Environment.DEMO,
        Environment.LOCAL
      ],
      control: { type: "radio" }
    }
  }
};

export const Default = (args) => {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const menuItems = (
    <>
      <Tooltip
        title={!drawerOpen ? "Some" : ""}
        placement="right"
        arrow
      >
        <Button onClick={action("Some was clicked!")}>Some</Button>
      </Tooltip>
      <Tooltip
        title={!drawerOpen ? "Cool" : ""}
        placement="right"
        arrow
      >
        <Button onClick={action("Cool was clicked!")}>Cool</Button>
      </Tooltip>
      <Tooltip
        title={!drawerOpen ? "Items" : ""}
        placement="right"
        arrow
      >
        <Button onClick={action("Items was clicked!")}>Items</Button>
      </Tooltip>
    </>
  );

  return (
    <GlobalAppBar
      getDrawerOpen={setDrawerOpen}
      {...{ menuItems, ...args }}
    >
      <LoremSuperLong />
    </GlobalAppBar>
  );
};

const iconMenuButtonProps = {
  icon: <AddBox />,
  menuItems: [
    {
      text: "New IOC Type",
      action: () => alert("New IOC Type")
    },
    {
      text: "Other Action",
      action: () => alert("Some other action"),
      icon: <BugReportIcon />
    }
  ]
};

const helpButtonProps = {
  icon: <HelpCenterIcon />,
  menuItems: [
    {
      text: "Help",
      action: () => alert("Help button pressed")
    }
  ]
};

Default.args = {
  defaultHomeButton: (
    <MuiIconButton
      onClick={action("Home was clicked!")}
      color="inherit"
    >
      <Home />
    </MuiIconButton>
  ),
  defaultTitle: "Example title",
  defaultActionButton: (
    <Button
      onClick={action("Login was clicked!")}
      color="primaryContrastText"
    >
      Login
    </Button>
  ),
  defaultHelpButton: <IconMenuButton {...helpButtonProps} />,
  defaultIconMenuButton: <IconMenuButton {...iconMenuButtonProps} />,
  widthOpen: "241px",
  environment: Environment.PROD
};

export const Banner = (args) => {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const menuItems = (
    <>
      <Tooltip
        title={!drawerOpen ? "Some" : ""}
        placement="right"
        arrow
      >
        <Button onClick={action("Some was clicked!")}>Some</Button>
      </Tooltip>
      <Tooltip
        title={!drawerOpen ? "Cool" : ""}
        placement="right"
        arrow
      >
        <Button onClick={action("Cool was clicked!")}>Cool</Button>
      </Tooltip>
      <Tooltip
        title={!drawerOpen ? "Items" : ""}
        placement="right"
        arrow
      >
        <Button onClick={action("Items was clicked!")}>Items</Button>
      </Tooltip>
    </>
  );

  return (
    <GlobalAppBar
      getDrawerOpen={setDrawerOpen}
      {...{ menuItems, ...args }}
    >
      <>
        <BodyBox
          sx={{
            width: "100%",
            maxWidth: "1400px",
            paddingBottom: theme.spacing(2)
          }}
        >
          <MaintenanceModeBanner message={"Maintenance Message"} />
        </BodyBox>
        <LoremSuperLong />
      </>
    </GlobalAppBar>
  );
};
Banner.args = {
  ...Default.args
};
