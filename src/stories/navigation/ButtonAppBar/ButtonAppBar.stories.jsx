import { Home } from "@mui/icons-material";
import { Button, IconButton as MuiIconButton } from "@mui/material";
import HelpCenterIcon from "@mui/icons-material/HelpCenter";
import { ButtonAppBar } from "../../../components/navigation/ButtonAppBar";
import { IconMenuButton } from "../../../components/common/IconMenuButton";
import { sbDisabledArg } from "../../utils/common-args";
import { Environment } from "../../../components/navigation/Environment";

export default {
  title: "Navigation/ButtonAppBar",
  argTypes: {
    defaultHomeButton: sbDisabledArg,
    defaultActionButton: sbDisabledArg,
    defaultIconMenuButton: sbDisabledArg,
    defaultHelpButton: sbDisabledArg,
    environment: {
      options: [
        Environment.PROD,
        Environment.TEST,
        Environment.DEMO,
        Environment.LOCAL
      ],
      control: { type: "radio" }
    }
  }
};

export const Default = (args) => <ButtonAppBar {...args} />;
Default.args = {
  environment: Environment.PROD
};

export const Example = (args) => <ButtonAppBar {...args} />;
const iconMenuButtonProps = {
  menuItems: [
    {
      text: "New IOC Type",
      action: () => alert("New IOC Type")
    }
  ]
};
const helpButtonProps = {
  icon: <HelpCenterIcon />,
  menuItems: [
    {
      text: "Help",
      action: () => alert("Help button pressed")
    }
  ]
};
Example.args = {
  defaultHomeButton: (
    <MuiIconButton color="inherit">
      <Home />
    </MuiIconButton>
  ),
  title: "Example title",
  /** Important:
   * The 'outlined' variant uses the primary color instead of contrast color!
   * Pair the "outlined" variant with the primaryInverted color, otherwise
   * the button will appear to be invisible against the ButtonAppBar
   * background color.
   */
  defaultActionButton: <Button color="primaryContrastText">Login</Button>,
  defaultHelpButton: <IconMenuButton {...helpButtonProps} />,
  defaultIconMenuButton: <IconMenuButton {...iconMenuButtonProps} />,
  environment: Environment.PROD
};
