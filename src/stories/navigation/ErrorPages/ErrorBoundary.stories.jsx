import { useState } from "react";
import { Button, Container } from "@mui/material";
import { BrowserRouter } from "react-router-dom";
import { AppErrorBoundary } from "../../../components/navigation/ErrorBoundary/ErrorBoundary";

export default {
  title: "Navigation/ErrorBoundary"
};

const Break = () => {
  const [broken, setBroken] = useState(false);
  const Bomb = ({ where }) => {
    throw new Error(
      `KABOOM: You broke the UI ${where}, dismiss the react error overlay if in Storybook/dev-mode to see fallback page`
    );
  };
  return (
    <Container sx={{ gap: 2, display: "flex", justifyContent: "center" }}>
      <Button
        variant="contained"
        onClick={() => setBroken(true)}
      >
        Break the UI during render
      </Button>
      {broken && <Bomb where="during Render" />}
      <Button
        variant="contained"
        onClick={() => {
          Bomb({ where: "in a Callback" });
        }}
      >
        Break the UI asynchronously
      </Button>
    </Container>
  );
};

const Template = ({ ...args }) => {
  return (
    <BrowserRouter>
      <AppErrorBoundary {...args}>
        <Break />
      </AppErrorBoundary>
    </BrowserRouter>
  );
};

export const Default = (args) => <Template {...args} />;
Default.args = {
  supportHref:
    "https://jira.esss.lu.se/plugins/servlet/desk/portal/44?requestGroup=137"
};
