import { Paper } from "@mui/material";
import { BrowserRouter } from "react-router-dom";
import { ServerErrorPage } from "../../../components/navigation/ServerErrorPage";
import {
  SidewaysMascot,
  VerticalMascot
} from "../../../components/common/Mascot/Mascot";

export default {
  title: "Navigation/ServerErrorPage",
  component: ServerErrorPage
};

const Template = (props) => {
  return (
    <BrowserRouter>
      <Paper sx={{ padding: 2 }}>
        <ServerErrorPage {...props} />
      </Paper>
    </BrowserRouter>
  );
};

const mascotDefaultProps = { width: "500px", height: "400px" };

export const Default = (args) => <Template {...args} />;
Default.args = {
  status: "404",
  supportHref:
    "https://jira.esss.lu.se/plugins/servlet/desk/portal/44?requestGroup=137",
  errorPageProps: {
    mascotVariantL: <SidewaysMascot {...mascotDefaultProps} />,
    mascotVariantR: <VerticalMascot {...mascotDefaultProps} />
  }
};

const CustomMessagesTemplate = ({ message, status, supportHref }) => {
  const STATUS = {
    404: `Template type (ID: ${status}) was not found`,
    403: "You cannot modify templates!",
    401: "Login to manage templates",
    418: "This template is a teapot 🍵",
    400: `Template type (ID: ${status}) is not a valid id`,
    503: "Gitlab was too slow 😩"
  };

  return (
    <Template
      message={STATUS[status] ?? message}
      status={status}
      supportHref={supportHref}
    />
  );
};

export const CustomMessages = (args) => <CustomMessagesTemplate {...args} />;
CustomMessages.args = {
  // message: "Page not found",
  status: "404",
  supportHref:
    "https://jira.esss.lu.se/plugins/servlet/desk/portal/44?requestGroup=137"
};
