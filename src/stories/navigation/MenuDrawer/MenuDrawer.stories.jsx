import { Button } from "@mui/material";
import { useCallback, useState } from "react";
import { action } from "@storybook/addon-actions";
import { MenuDrawer } from "../../../components/navigation/MenuDrawer";

export default {
  title: "Navigation/MenuDrawer"
};

const Template = (args) => {
  const [open, setOpen] = useState(false); // mirror the default in the component
  const toggleDrawer = useCallback(() => {
    setOpen(!open);
  }, [open]);

  return (
    <MenuDrawer {...{ open, toggleDrawer, ...args }}>
      <Button onClick={action("Some was clicked!")}>Some</Button>
      <Button onClick={action("Cool was clicked!")}>Cool</Button>
      <Button onClick={action("Items was clicked!")}>Items</Button>
    </MenuDrawer>
  );
};

export const Default = (args) => <Template {...args} />;

export const CustomWidth = (args) => <Template {...args} />;

CustomWidth.args = {
  widthOpen: "50%",
  widthClosed: "10%"
};
