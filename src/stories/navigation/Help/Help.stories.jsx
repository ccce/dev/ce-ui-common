import { useState } from "react";
import { Button, Paper, Tooltip, Stack, Typography } from "@mui/material";
import AutoAwesomeIcon from "@mui/icons-material/AutoAwesome";
import { Help } from "../../../components/navigation/Help";
import { GlobalAppBar } from "../../../components/navigation/GlobalAppBar";

export default {
  title: "Navigation/Help",
  component: Help
};

const Template = (args) => {
  return <Help {...args} />;
};

const HelpContainer = (args) => {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const menuItems = (
    <>
      <Tooltip
        title={!drawerOpen ? "Hmm" : ""}
        placement="right"
        arrow
      >
        <Button>Some Item</Button>
      </Tooltip>
    </>
  );

  return (
    <GlobalAppBar
      getDrawerOpen={setDrawerOpen}
      menuItems={menuItems}
      defaultTitle="Example title"
    >
      <Paper sx={{ padding: 2 }}>
        <Template {...args} />
      </Paper>
    </GlobalAppBar>
  );
};

export const Default = (args) => <HelpContainer {...args} />;
Default.args = {
  summary:
    "Introducing 'common-ui' - the app that will have you saying, \"Wow, I could've built that myself!\" It's not just a library, it's a react component extravaganza! With over a hundred pre-built components, you'll spend less time coding and more time marveling at the wonders of reusability. But don't worry, we won't tell anyone you didn't write it from scratch. Shh!",
  docsHref: "https://confluence.esss.lu.se",
  supportHref: "https://jira.esss.lu.se/plugins/servlet/desk",
  version: "0.1.2"
};

export const ComponentAsSummary = (args) => <HelpContainer {...args} />;
ComponentAsSummary.args = {
  ...Default.args,
  summary: (
    <Stack gap={1}>
      <Typography>
        Experience the future of coding with common-ui, a game-changing React
        component library. Imagine components that intuitively adapt, turning
        you into a digital wizard crafting stunning interfaces effortlessly. Say
        goodbye to ordinary development and embrace an era of innovation,
        efficiency, and pure coding exhilaration.
      </Typography>
      <Typography>
        common-ui&apos;s quantum components redefine building apps. They{" "}
        <Typography
          fontStyle="italic"
          fontWeight="bold"
          component="span"
        >
          dynamically morph for different screens and devices
        </Typography>
        , erasing compatibility woes. Witness components that bend to your will,
        eliminating redundancy and boosting performance. With common-ui, coding
        reaches new frontiers.
      </Typography>
      <Typography>
        Embark on a coding odyssey with common-ui as your guide. Its
        user-friendly API and comprehensive tutorials empower both novices and
        experts. The future has arrived, and it&apos;s common-ui—a portal to
        limitless coding possibilities. Your quantum coding adventure starts
        now! <AutoAwesomeIcon fontSize="small" />
      </Typography>
    </Stack>
  )
};
