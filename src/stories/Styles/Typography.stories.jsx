import { Paper, ThemeProvider, Typography } from "@mui/material";
import { theme } from "../../style/Theme";

export default {
  title: "Styles/Typography",
  parameters: {
    withThemeProvider: false
  }
};

const TypographyElements = ({ fontWeight }) => {
  return (
    <Paper sx={{ padding: 2 }}>
      <Typography
        variant="h1"
        fontWeight={fontWeight}
      >
        Header 1
      </Typography>
      <Typography
        variant="h2"
        fontWeight={fontWeight}
      >
        Header 2
      </Typography>
      <Typography
        variant="h3"
        fontWeight={fontWeight}
      >
        Header 3
      </Typography>
      <Typography
        variant="body1"
        fontStyle="italic"
        fontWeight={fontWeight}
      >
        (Variants h4-h6 are disabled)
      </Typography>
      <Typography
        variant="body1"
        fontWeight={fontWeight}
      >
        Body 1
      </Typography>
      <Typography
        variant="body2"
        fontWeight={fontWeight}
      >
        Body 2
      </Typography>
      <Typography
        variant="subtitle1"
        fontWeight={fontWeight}
      >
        Subtitle 1
      </Typography>
      <Typography
        variant="subtitle2"
        fontWeight={fontWeight}
      >
        Subtitle 2
      </Typography>
      <Typography
        variant="button"
        fontWeight={fontWeight}
        sx={{ display: "block" }}
      >
        Button
      </Typography>
      <Typography
        variant="overline"
        fontWeight={fontWeight}
        sx={{ display: "block" }}
      >
        Overline
      </Typography>
      <Typography
        variant="caption"
        fontWeight={fontWeight}
      >
        Caption
      </Typography>
      <Typography
        variant="code"
        fontWeight={fontWeight}
        sx={{ display: "block" }}
      >
        Code (Monospace)
      </Typography>
    </Paper>
  );
};

const Template = (args) => {
  const { withStyle, fontWeight } = args;

  return withStyle ? (
    <ThemeProvider theme={theme}>
      <TypographyElements {...{ fontWeight }} />
    </ThemeProvider>
  ) : (
    <TypographyElements {...{ fontWeight }} />
  );
};

export const Default = (args) => <Template {...args} />;
Default.args = {
  withStyle: true,
  fontWeight: undefined
};
Default.argTypes = {
  fontWeight: {
    options: [undefined, "light", "regular", "medium", "bold"],
    control: { type: "radio" }
  }
};
