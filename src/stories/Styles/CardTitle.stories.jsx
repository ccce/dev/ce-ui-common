import { Card, CardContent, CardHeader, Typography } from "@mui/material";
import { Lorem } from "../utils/common-components";

export default {
  title: "Styles/CardTitle"
};

const Template = (args) => (
  <>
    <Typography variant="h1">Example title</Typography>
    <Card>
      <CardHeader
        title="Placeholder for card title"
        titleTypographyProps={{
          variant: args.typograhpyVariant,
          component: "h2"
        }}
      />
      <CardContent>
        <Lorem />
      </CardContent>
    </Card>
  </>
);

export const CardTitleVariant = (args) => <Template {...args} />;
CardTitleVariant.args = {
  typograhpyVariant: "h1"
};
CardTitleVariant.argTypes = {
  typograhpyVariant: {
    options: [
      "h1",
      "h2",
      "h3",
      "subtitle1",
      "subtitle2",
      "body1",
      "body2",
      "caption"
    ],
    control: { type: "radio" }
  }
};
