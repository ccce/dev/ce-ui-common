import { Grid, Paper, Typography } from "@mui/material";
import { theme } from "../../style/Theme";

export default {
  title: "Styles/Colors"
};

const ColorCard = ({ color, name, description, textColor }) => {
  return (
    <Paper
      sx={{
        backgroundColor: color,
        color: textColor ? textColor : "essWhite.main",
        padding: 2
      }}
    >
      <Typography variant="h2">{name}</Typography>
      <Typography variant="p">{description}</Typography>
    </Paper>
  );
};

const getColor = (palette, path) => {
  const elems = path.split(".");
  let current = palette;
  elems.forEach((k) => {
    if (current[k] !== undefined) {
      current = current[k];
    }
  });
  return current;
};

const Template = ({ title, colors }) => (
  <Grid
    container
    spacing={2}
    sx={{ padding: 2 }}
    flexDirection="column"
  >
    <Grid item>
      <Typography variant="h1">{title}</Typography>
    </Grid>
    {Object.entries(colors).map(([k, v]) => (
      <Grid
        item
        key={k}
      >
        <ColorCard
          color={getColor(theme.palette, k)}
          name={v.name}
          description={v.description}
          textColor={v.textColor}
        />
      </Grid>
    ))}
  </Grid>
);

const primaryColors = {
  "primary.main": {
    name: "Primary/Cyan",
    description: "Primary color; must be used whenever communicating 'ESS'"
  },
  "secondary.main": {
    name: "Secondary",
    description: `Secondary color; This deep blue is inspired by the sea
    which connects Sweden and Denmark,
    the host states.`
  },
  "essWhite.main": {
    name: "White",
    description:
      "Used with the logotype as a background color, provides the best contrast for text.",
    textColor: "essBlack"
  },
  "essBlack.main": {
    name: "Black",
    description:
      "Used with the logotype as a negative background color, provides the best contrast for text."
  }
};

const complementaryColors = {
  "essPurple.main": {
    name: "Purple",
    description:
      "This colour represents sophistication and elegance, coupled with creativity and calmness."
  },
  "essOrange.main": {
    name: "Orange",
    description:
      "As well as being the combination of the red and yellow in the home countries’ flags, orange also symbolises joy, frugality and safety."
  },
  "essForest.main": {
    name: "Forest",
    description:
      "The forest colour can often be found on barn doors throughout the region local to ESS, and represents stability and humility."
  },
  "essGrass.main": {
    name: "Grass",
    description:
      "The light green colour of grass mirrors the beech trees typical to this part of Europe, and symbolises hope and new beginnings."
  },
  "essNavy.main": {
    name: "Navy",
    description:
      "This deep blue is inspired by the sea which connects Sweden and Denmark, the host states."
  }
};

const statusColors = {
  "status.ok.main": {
    name: "OK",
    description: "Indicates a positive result from an action"
  },
  "status.progress.main": {
    name: "Progress",
    description: "Indicates a pending result from an action"
  },
  "status.fail.main": {
    name: "Fail",
    description: "Indicates a failure from an action"
  },
  "status.disabled.main": {
    name: "Disabled",
    description: "Indicates a disabled action"
  }
};

export const Default = (args) => (
  <Template
    title="Primary Colors"
    colors={primaryColors}
    {...args}
  />
);
export const ComplementaryColors = (args) => (
  <Template
    title="Complementary Colors"
    colors={complementaryColors}
    {...args}
  />
);
export const StatusColors = (args) => (
  <Template
    title="Status Colors"
    colors={statusColors}
    {...args}
  />
);
