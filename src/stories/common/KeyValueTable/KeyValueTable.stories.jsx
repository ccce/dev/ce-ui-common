import { Circle } from "@mui/icons-material";
import { Container, IconButton, Paper } from "@mui/material";
import { KeyValueTable } from "../../../components/common/KeyValueTable";
import { sbDisabledArg } from "../../utils/common-args";
import * as obj_list from "../../../mocks/fixtures/api/hosts.json";

export default {
  title: "Common/KeyValueTable"
};

const obj = obj_list.default[0];
obj.sensor = (
  <IconButton>
    <Circle sx={{ color: "red" }} />
  </IconButton>
);

const Template = (args) => (
  <Container sx={{ display: "flex" }}>
    <Paper sx={{ display: "flex", width: "100%", padding: 2 }}>
      <KeyValueTable {...args} />
    </Paper>
  </Container>
);

export const TableVariant = (args) => <Template {...args} />;
TableVariant.args = {
  obj,
  variant: "table"
};
// Disable interactively changing the obj argument in Storybook
TableVariant.argTypes = {
  obj: sbDisabledArg
};

export const TableLoading = (args) => <Template {...args} />;
TableLoading.args = {
  obj: null,
  variant: "table",
  loading: true
};
// Disable interactively changing the obj argument in Storybook
TableLoading.argTypes = {
  obj: sbDisabledArg
};

export const TableCustomized = (args) => <Template {...args} />;
TableCustomized.args = {
  obj,
  variant: "table",
  sx: {
    color: "white",
    backgroundColor: "essNavy.main"
  }
};
// Disable interactively changing the obj argument in Storybook
TableCustomized.argTypes = {
  obj: sbDisabledArg
};

export const OverlineVariant = (args) => <Template {...args} />;
OverlineVariant.args = {
  obj,
  variant: "overline"
};
OverlineVariant.argTypes = {
  obj: sbDisabledArg
};

export const OverlineLoading = (args) => <Template {...args} />;
OverlineLoading.args = {
  obj: null,
  variant: "overline",
  loading: true
};
OverlineLoading.argTypes = {
  obj: sbDisabledArg
};

export const OverlineCustomized = (args) => <Template {...args} />;
OverlineCustomized.args = {
  obj,
  variant: "overline",
  sx: {
    color: "white",
    backgroundColor: "essNavy.main"
  }
};
OverlineCustomized.argTypes = {
  obj: sbDisabledArg
};
