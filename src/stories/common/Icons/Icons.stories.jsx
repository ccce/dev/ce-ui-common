import { Stack, Typography } from "@mui/material";
import {
  IconRunning,
  IconFailed,
  IconSuccessful,
  IconSkipped,
  IconUnknown
} from "../../../components/common/Icons/Icons";

export default {
  title: "Common/Icons"
};

export const Default = () => (
  <Stack gap={1}>
    <Stack
      flexDirection="row"
      gap={1}
    >
      <IconSkipped size="small" />
      <Typography>Skipped</Typography>
    </Stack>
    <Stack
      flexDirection="row"
      gap={1}
    >
      <IconSuccessful size="medium" />
      <Typography>Successful</Typography>
    </Stack>
    <Stack
      flexDirection="row"
      gap={1}
    >
      <IconFailed size="medium" />
      <Typography>Failed</Typography>
    </Stack>
    <Stack
      flexDirection="row"
      gap={1}
    >
      <IconUnknown size="small" />
      <Typography>Unknown</Typography>
    </Stack>
    <Stack
      flexDirection="row"
      gap={1}
    >
      <IconRunning size="medium" />
      <Typography>Running</Typography>
    </Stack>
  </Stack>
);
