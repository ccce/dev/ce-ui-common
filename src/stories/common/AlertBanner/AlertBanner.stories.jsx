import {
  AlertBanner,
  AlertBannerList
} from "../../../components/common/AlertBanner";
import { sbDisabledArg } from "../../utils/common-args";

export default {
  title: "Common/AlertBanner",
  component: AlertBanner
};

export const Default = (args) => <AlertBanner {...args} />;
Default.args = {
  type: "warning",
  message: "Your custom message here"
};

export const WithLink = (args) => <AlertBanner {...args} />;
WithLink.argTypes = Default.argTypes;
WithLink.args = {
  ...Default.args,
  link: "#"
};

export const ManyAlerts = (args) => <AlertBannerList {...args} />;
ManyAlerts.argTypes = {
  alerts: sbDisabledArg,
  type: sbDisabledArg,
  message: sbDisabledArg,
  link: sbDisabledArg
};
ManyAlerts.args = {
  alerts: [
    {
      type: "info",
      message: "Your info message here"
    },
    {
      type: "warning",
      message: "Your warning message here"
    },
    {
      type: "error",
      message: "Your error message here",
      link: "#"
    }
  ]
};
