import { Paper } from "@mui/material";
import { AcUnit, Warning } from "@mui/icons-material";
import { IconBadge } from "../../../components/common/IconBadge";
import { useUniqueKeys } from "../../../hooks/useUniqueKeys";

export default {
  title: "Common/IconBadge"
};

const iconBadges = [
  {
    icon: <AcUnit />,
    title: "It's really cold outside!",
    subtitle: "Dangerously so!"
  },
  {
    icon: <AcUnit />,
    secondaryIcon: <Warning />,
    title: "It's really cold outside!",
    subtitle: "Dangerously so!"
  },
  {
    icon: <AcUnit />,
    title: "It's really cold outside!"
  },
  {
    icon: <AcUnit />,
    secondaryIcon: <Warning />,
    title: "It's really cold outside!"
  }
];

export const Default = () => {
  const iconBadgesKeys = useUniqueKeys(iconBadges);
  return (
    <>
      {iconBadges.map((badge, i) => {
        return (
          <Paper
            key={iconBadgesKeys[i]}
            sx={{
              padding: 2,
              marginBottom: 2
            }}
          >
            <IconBadge {...badge} />
          </Paper>
        );
      })}
    </>
  );
};
