import { Button, Typography } from "@mui/material";
import { action } from "@storybook/addon-actions";
import { SimpleAccordion } from "../../../components/common/SimpleAccordion";
import { Lorem } from "../../utils/common-components";

export default {
  title: "Common/SimpleAccordion"
};

const Template = (args) => {
  const { defaultExpanded, summary } = args;
  return (
    <SimpleAccordion
      defaultExpanded={defaultExpanded}
      summary={summary}
      {...args}
    >
      <Lorem />
    </SimpleAccordion>
  );
};

export const DefaultExpanded = (args) => <Template {...args} />;
DefaultExpanded.args = {
  defaultExpanded: true,
  summary: "Sample accordion"
};

export const TextContent = (args) => <Template {...args} />;
TextContent.args = {
  defaultExpanded: false,
  summary: "Sample accordion"
};

export const ComplexContent = () => (
  <SimpleAccordion summary="Sample accordion">
    <Lorem />
    <Button
      onClick={action("Button was clicked!")}
      variant="contained"
    >
      Button
    </Button>
  </SimpleAccordion>
);

export const Elevated = ({ elevation }) => (
  <SimpleAccordion
    elevation={elevation}
    summary="Sample accordion"
  >
    <Lorem />
    <Button
      onClick={action("Button was clicked!")}
      variant="contained"
    >
      Button
    </Button>
  </SimpleAccordion>
);
Elevated.args = {
  elevation: 1
};
Elevated.argTypes = {
  elevation: {
    control: {
      type: "number",
      min: 0
    },
    name: "Elevation"
  }
};

const CustomSummaryComponent = () => (
  <Typography variant="h2">With a custom summary component</Typography>
);

export const CustomSummary = (args) => {
  return (
    <Template
      {...args}
      summary={<CustomSummaryComponent />}
    />
  );
};
