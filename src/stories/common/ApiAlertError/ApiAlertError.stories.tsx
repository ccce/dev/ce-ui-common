import { Meta } from "@storybook/react";
import {
  ApiAlertError,
  ApiError
} from "../../../components/common/ApiAlertError";

export default {
  title: "Common/ApiAlertError",
  component: ApiAlertError
} as Meta<typeof ApiAlertError>;

interface ApiAlertErrorProps {
  error: ApiError;
}

export const Default = ({ error }: ApiAlertErrorProps) => (
  <ApiAlertError error={error} />
);

Default.args = {
  error: {
    status: 404,
    data: {
      error: "Not Found",
      description: "The requested resource could not be found."
    }
  }
};

export const BrowserError = ({ error }: ApiAlertErrorProps) => (
  <ApiAlertError error={error} />
);

BrowserError.args = {
  error: {
    status: "FETCH_ERROR",
    error: "TypeError: Failed to fetch"
  }
};

export const SerializedError = ({ error }: ApiAlertErrorProps) => (
  <ApiAlertError error={error} />
);

SerializedError.args = {
  error: {
    code: "503",
    message: "Internal server error"
  }
};

export const FallbackEmptyError = ({ error }: ApiAlertErrorProps) => (
  <ApiAlertError error={error} />
);

FallbackEmptyError.args = {
  error: {
    code: "405",
    message: null
  }
};
