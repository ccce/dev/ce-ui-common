import { MemoryRouter } from "react-router-dom";
import PetsIcon from "@mui/icons-material/Pets";
import {
  ExternalButtonLink,
  ExternalLink,
  InternalButtonLink,
  InternalLink
} from "../../../components/common/Link";

export default {
  title: "Common/Link"
};

const InternalTemplate = ({ children }) => {
  return <MemoryRouter>{children}</MemoryRouter>;
};
export const Internal = (args) => (
  <InternalTemplate>
    <InternalLink {...args}>{args.text}</InternalLink>
  </InternalTemplate>
);
Internal.args = {
  to: "/",
  text: "Navigate within app",
  label: "Navigate internally to somewhere"
};
export const InternalButton = (args) => (
  <InternalTemplate>
    <InternalButtonLink {...args}>{args.text}</InternalButtonLink>
  </InternalTemplate>
);
InternalButton.args = {
  ...Internal.args,
  variant: "contained",
  color: "primary"
};

const ExternalTemplate = ({ demoCustomIcon, render, ...props }) => {
  if (demoCustomIcon) {
    props = {
      ...props,
      ExternalLinkContentsProps: {
        Icon: PetsIcon,
        IconProps: { color: "essPurple" }
      }
    };
  }
  return render(props);
};

export const External = (args) => (
  <ExternalTemplate
    {...args}
    render={(props) => <ExternalLink {...props}>{props.text}</ExternalLink>}
  />
);
External.args = {
  disableExternalLinkIcon: false,
  demoCustomIcon: false,
  href: "https://europeanspallationsource.se/",
  newTab: true,
  text: "Visit External Website",
  label: "Visit our website"
};

export const ExternalButton = (args) => (
  <ExternalTemplate
    {...args}
    render={(props) => (
      <ExternalButtonLink {...props}>{props.text}</ExternalButtonLink>
    )}
  />
);
ExternalButton.args = {
  ...External.args,
  variant: "contained",
  color: "primary"
};
