import { Stack, Typography } from "@mui/material";
import { useUniqueKeys } from "../../../hooks/useUniqueKeys";
import {
  LinearStepper,
  SingleStateStepper,
  STEPPER_STATES,
  StepperComponents
} from "../../../components/common/Stepper";

export default {
  title: "Common/Stepper"
};

const stepperData = {
  steps: [
    {
      label: "Skipped step",
      state: STEPPER_STATES.SKIPPED
    },
    {
      label: "Successful step",
      state: STEPPER_STATES.SUCCESSFUL
    },
    {
      label: "Failed step",
      state: STEPPER_STATES.FAILED
    },
    {
      label: "Partial failure step",
      state: STEPPER_STATES.PARTIAL_FAILURE
    },
    {
      label: "Unknown step",
      state: STEPPER_STATES.UNKNOWN
    },
    {
      label: "Running step",
      state: STEPPER_STATES.RUNNING
    },
    {
      label: "Queued step",
      state: STEPPER_STATES.QUEUED
    }
  ],
  activeStep: 3,
  isCompleted: false
};

const Template = ({ children }) => {
  return (
    <Stack
      gap={2}
      justifyContent="space-between"
    >
      {children}
    </Stack>
  );
};

export const Linear = ({ ...args }) => (
  <Template>
    <Stack
      gap={1}
      sx={{ width: "200px" }}
    >
      <Typography variant="h3">Inline:</Typography>
      <LinearStepper
        steps={args.steps}
        activeStep={stepperData.activeStep}
        isCompleted={stepperData.isCompleted}
      />
    </Stack>
  </Template>
);

Linear.args = {
  steps: [
    { state: STEPPER_STATES.SKIPPED, label: "Gathering Ingredients" },
    { state: STEPPER_STATES.SUCCESSFUL, label: "Mixing Together" },
    { state: STEPPER_STATES.RUNNING, label: "Cooking" }
  ]
};

export const SingleStepper = ({ ...args }) => (
  <SingleStateStepper
    step={stepperData.steps[args.step]}
    showToolTip={args.showToolTip}
    bgSize={args.bgSize}
    iconSize={args.iconSize}
  />
);

SingleStepper.args = {
  step: 0,
  showToolTip: false,
  bgSize: "",
  iconSize: ""
};

SingleStepper.argTypes = {
  step: {
    options: [0, 1, 2, 3, 4, 5, 6],
    control: { type: "select" }
  }
};

export const Steps = ({ ...args }) => {
  const { steps } = stepperData;
  const smallStepKeys = useUniqueKeys(steps);
  return (
    <Stack gap={3}>
      {args.steps.map((step, i) => {
        const StepIcon = StepperComponents[step.state];
        return (
          <StepIcon
            key={smallStepKeys[i]}
            step={step}
            {...args}
          />
        );
      })}
    </Stack>
  );
};

Steps.args = {
  steps: stepperData.steps,
  showToolTip: true,
  bgSize: "",
  iconSize: ""
};
