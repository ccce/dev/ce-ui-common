import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { LabeledIcon } from "../../../components/common/LabeledIcon";

export default {
  title: "common/LabeledIcon",
  argTypes: {
    labelPosition: {
      options: ["none", "right", "left"],
      control: { type: "radio" }
    }
  }
};

const Template = (args) => {
  return (
    <LabeledIcon
      Icon={AccountCircleIcon}
      {...args}
    />
  );
};

export const Default = (args) => <Template {...args} />;
Default.args = {
  label: "Account",
  LabelProps: { fontWeight: "bold" },
  labelPosition: "right",
  IconProps: { color: "primary" }
};
