import { Typography } from "@mui/material";
import {
  AppBox,
  BodyBox,
  ContentBox,
  HeaderBox
} from "../../../components/common/AppLayout";
import { Lorem } from "../../utils/common-components";

export default {
  title: "Common/AppLayout"
};

export const Default = () => {
  return (
    <AppBox sx={{ height: "100%" }}>
      <HeaderBox sx={{ backgroundColor: "lightpink" }}>
        <Typography
          variant="h1"
          sx={{ fontSize: "1.2rem!important", flexGrow: "1" }}
          className="CeuiButtonAppBar-Title"
          noWrap
        >
          This is a Header
        </Typography>
      </HeaderBox>
      <BodyBox>
        <ContentBox sx={{ backgroundColor: "lightgreen" }}>
          <Typography variant="button">Some</Typography>
          <Typography variant="button">Menu</Typography>
          <Typography variant="button">Items</Typography>
        </ContentBox>
        <ContentBox sx={{ backgroundColor: "lemonchiffon" }}>
          <Lorem />
          <Lorem />
          <Lorem />
        </ContentBox>
      </BodyBox>
    </AppBox>
  );
};
