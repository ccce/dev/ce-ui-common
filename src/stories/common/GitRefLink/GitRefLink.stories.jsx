import { GitRefLink } from "../../../components/common/GitRefLink";

export default {
  title: "Common/GitRefLink"
};

const Template = () => {
  return (
    <GitRefLink
      url="https://gitlab.esss.lu.se/ccce/dev/iocs/instances/e3-ioc-test-01.git"
      revision="0.0.TEST"
    />
  );
};

export const Default = (args) => <Template {...args} />;
