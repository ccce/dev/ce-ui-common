import { Paper, Typography } from "@mui/material";
import { EmptyValue } from "../../../components/common/EmptyValue";

export default {
  title: "Common/EmptyValue"
};

const Template = () => {
  return (
    <Paper sx={{ height: "100vh", padding: 2 }}>
      <Typography>The following value is empty.</Typography>
      <Paper
        sx={{ padding: 1, width: "190px" }}
        variant="outlined"
      >
        <EmptyValue />
      </Paper>
    </Paper>
  );
};

export const Default = () => <Template />;
