import { Box } from "@mui/material";
import { EssIconLogo } from "../../../components/common/Logo";
import { theme } from "../../../style/Theme";

export default {
  title: "Common/Logo"
};

export const IconLogoLight = (args) => (
  <Box
    sx={{
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "darkgrey",
      width: `${args.size ? `${args.size * 2}px` : 72}`,
      height: `${args.size ? `${args.size * 2}px` : 72}`
    }}
  >
    <EssIconLogo {...args} />
  </Box>
);

IconLogoLight.args = {
  size: 70
};

export const IconLogoPrimaryAccent = (args) => <EssIconLogo {...args} />;

IconLogoPrimaryAccent.args = {
  size: 70,
  fill: theme.palette.primary.main
};
