import {
  Duration,
  StartAndDuration
} from "../../../components/common/Duration";

export default {
  title: "Common/Duration"
};
const startDate = new Date("2023-05-31T12:46:47.021");
const finishDate = new Date("2023-05-31T13:46:47.021");

const Template = (args) => {
  return <Duration {...args} />;
};

export const Default = (args) => <Template {...args} />;
Default.args = {
  createOrStartDate: startDate,
  finishedAt: finishDate,
  textOnly: false
};

export const NoFinish = (args) => <Template {...args} />;
NoFinish.args = {
  ...Default.args,
  finishedAt: undefined
};

export const NoRender = (args) => <Template {...args} />;
NoRender.args = {
  ...Default.args,
  textOnly: true
};

const StartAndDurationTemplate = (args) => {
  return <StartAndDuration {...args} />;
};

export const DefaultStartAndDuration = (args) => (
  <StartAndDurationTemplate {...args} />
);
DefaultStartAndDuration.args = {
  startTime: startDate,
  createdAt: undefined,
  finishedAt: finishDate,
  textOnly: false
};

export const NoRenderStartAndDuration = (args) => (
  <StartAndDurationTemplate {...args} />
);
NoRenderStartAndDuration.args = {
  ...DefaultStartAndDuration.args,
  textOnly: true
};

export const NoFinishedStartAndDuration = (args) => (
  <StartAndDurationTemplate {...args} />
);
NoFinishedStartAndDuration.args = {
  ...DefaultStartAndDuration.args,
  finishedAt: undefined
};

export const CreatedStartAndDuration = (args) => (
  <StartAndDurationTemplate {...args} />
);
CreatedStartAndDuration.args = {
  ...DefaultStartAndDuration.args,
  createdAt: startDate,
  startTime: undefined
};
