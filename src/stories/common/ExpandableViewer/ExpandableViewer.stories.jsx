import {
  Grid,
  Stack,
  Typography,
  Autocomplete,
  TextField
} from "@mui/material";
import {
  ExpandableViewer,
  ExpandableViewerButton
} from "../../../components/common/ExpandableViewer";
import { Lorem } from "../../utils/common-components";

export default {
  title: "Common/ExpandableViewer"
};

const CustomTitle = ({ title, onClick, expandButtonText }) => {
  return (
    <Stack
      marginBottom={1}
      gap={1}
    >
      <Typography variant="h3">{title}</Typography>
      <Stack
        flexDirection="row"
        justifyContent="space-between"
        alignContent="center"
      >
        <Autocomplete
          size="small"
          disablePortal
          id="example"
          options={[
            { label: "Lorem", value: "lorem" },
            { label: "Ipsum", value: "ipsum" },
            { label: "Dolor", value: "dolor" }
          ]}
          sx={{ width: 100 }}
          renderInput={(params) => (
            <TextField
              {...params}
              label="Lorem"
            />
          )}
        />
        <ExpandableViewerButton {...{ onClick, expandButtonText }} />
      </Stack>
    </Stack>
  );
};

const Template = ({ useCustomTitleRender, ...args }) => {
  const renderTitle = useCustomTitleRender ? CustomTitle : null;

  return (
    <Grid
      container
      spacing={1}
    >
      <Grid
        item
        xs={5}
        md={5}
      >
        <ExpandableViewer
          renderTitle={renderTitle}
          {...args}
        >
          <Lorem />
          <Lorem />
          <Lorem />
        </ExpandableViewer>
      </Grid>
    </Grid>
  );
};
export const Default = (args) => <Template {...args} />;
Default.args = {
  loading: false,
  title: "Example",
  expandButtonText: "Popout",
  useCustomTitleRender: false
};
