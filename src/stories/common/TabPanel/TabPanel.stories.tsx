import { SyntheticEvent, useState } from "react";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import {
  Checkbox,
  FormControl,
  FormControlLabel,
  IconButton,
  Stack,
  Tab,
  Tabs,
  Typography
} from "@mui/material";
import { TabPanel } from "../../../components/common/TabPanel";
import { RootPaper } from "../../../components/common/container/RootPaper";

export default {
  title: "Common/TabPanel"
};

// This story demonstrates that for e.g. simply performing an action
// when clicking static tabs, the default MUI component is sufficient
export const SimpleTabs = () => {
  const [tab, setTab] = useState(0);

  const onTabChange = (_: SyntheticEvent, tab: number) => {
    console.debug(`You clicked tab ${tab}`);
    setTab(tab);
  };

  return (
    <RootPaper>
      <Tabs
        value={tab}
        onChange={onTabChange}
      >
        <Tab label="Tab 1" />
        <Tab label="Tab 2" />
        <Tab label="Tab 3" />
      </Tabs>
      <Typography>Unchanging Content</Typography>
      <Typography fontStyle="italic">
        (check dev console for onTabChange user messages)
      </Typography>
    </RootPaper>
  );
};

// However, when tabs need to be shown conditionally then it makes sense
// to use this custom tab panel component
export const ConditionalTabsTemplate = () => {
  console.debug("rendering");
  const [showTab2, setShowTab2] = useState(true);
  const [showTab3, setShowTab3] = useState(true);

  const tabs = [
    {
      label: "Tab 1",
      content: <Typography>This tab is not conditional</Typography>
    }
  ];
  if (showTab2) {
    tabs.push({
      label: "Tab 2",
      content: <Typography>Tab 2 can be shown/hidden conditionally</Typography>
    });
  }
  if (showTab3) {
    tabs.push({
      label: "Tab 3",
      content: (
        <Typography>
          The third tab can also be shown/hidden conditionally
        </Typography>
      )
    });
  }

  const onTabChange = (index: number, label?: string) => {
    console.debug(
      `Tab changed to tab with label '${label}' and index ${index}`
    );
  };

  return (
    <RootPaper>
      <FormControl>
        <FormControlLabel
          value="tab2"
          label="Show/Hide Tab 2"
          labelPlacement="end"
          control={
            <Checkbox
              checked={showTab2}
              onChange={(event) => setShowTab2(event.target.checked)}
            />
          }
        />
        <FormControlLabel
          value="tab3"
          label="Show/Hide Tab 3"
          labelPlacement="end"
          control={
            <Checkbox
              checked={showTab3}
              onChange={(event) => setShowTab3(event.target.checked)}
            />
          }
        />
      </FormControl>
      <TabPanel
        tabs={tabs}
        onTabChange={onTabChange}
        TabsProps={{
          "aria-label": "Demonstration Tabs Panel",
          centered: true,
          sx: { flex: 1 }
        }}
        renderTabs={(tabs) => {
          return (
            <Stack
              flexDirection="row"
              justifyContent="space-between"
            >
              <IconButton
                color="inherit"
                onClick={() => alert("You clicked me!")}
                size="large"
              >
                <ArrowBackIcon />
              </IconButton>
              {tabs}
            </Stack>
          );
        }}
      />
    </RootPaper>
  );
};
