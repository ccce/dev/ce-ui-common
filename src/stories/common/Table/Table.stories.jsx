import { useEffect } from "react";
import { Link } from "@mui/material";
import { usePagination } from "../../../hooks/usePagination";
import { Table } from "../../../components/common/Table";
import { RootPaper } from "../../../components/common/container/RootPaper";
import { GlobalAppBar } from "../../../components/navigation/GlobalAppBar";
import { columns } from "../../utils/common-data";
import { sbDisabledArg } from "../../utils/common-args";
import { EssThemeProvider } from "../../../providers/EssThemeProvider";
import hosts_data from "../../../mocks/fixtures/api/hosts.json";
import { withPagination } from "../../utils/withPagination";

export default {
  title: "Common/Table"
};

const Harness = ({ children }) => (
  <EssThemeProvider>
    <GlobalAppBar
      getDrawerOpen={() => {
        console.debug("drawer open");
      }}
      defaultTitle="Tables"
    >
      <RootPaper>{children}</RootPaper>
    </GlobalAppBar>
  </EssThemeProvider>
);

const PaginatableTable = (args) => {
  const { storybookEnablePagination, loading, empty, ...rest } = args;

  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: [5, 10, 20, 50]
  });

  let data = hosts_data;

  let value = empty
    ? undefined
    : withPagination({
        page: pagination.page ?? 0,
        limit: pagination.limit ?? 99999,
        data: data
      });

  useEffect(
    () => setPagination({ totalCount: value?.totalCount ?? 0 }),
    [setPagination, value?.totalCount]
  );

  const rows = value?.data?.map((row) => ({
    ...row,
    name: <Link>{row.name}</Link>
  }));

  let tableParams = { rows, loading };
  if (storybookEnablePagination) {
    tableParams = { ...tableParams, onPage: setPagination, pagination };
  }
  tableParams = { ...tableParams, ...rest };

  return <Table {...tableParams} />;
};

const Template = (args) => {
  return (
    <Harness>
      <PaginatableTable {...args} />
    </Harness>
  );
};

export const Default = (args) => <Template {...args} />;
Default.args = {
  columns,
  empty: false,
  striped: true,
  loadingRowCount: 5,
  disableHover: true,
  enableFilterToolbar: false,
  storybookEnablePagination: false // This is specific to these stories!
};
Default.argTypes = {
  columns: sbDisabledArg,
  rows: sbDisabledArg,
  rowClassName: sbDisabledArg
};

export const DefaultWithPagination = (args) => <Template {...args} />;
DefaultWithPagination.args = {
  ...Default.args,
  storybookEnablePagination: true
};
DefaultWithPagination.argTypes = {
  ...Default.argTypes
};

export const Empty = (args) => <Template {...args} />;
Empty.args = {
  ...Default.args,
  empty: true
};
Empty.argTypes = {
  ...Default.argTypes
};
Empty.parameters = {
  empty: true
};
export const InitialLoad = (args) => <Template {...args} />;
InitialLoad.args = {
  ...Default.args,
  loading: true
};
InitialLoad.argTypes = {
  ...Default.argTypes,
  loading: sbDisabledArg
};
InitialLoad.parameters = { ...Empty.parameters };
