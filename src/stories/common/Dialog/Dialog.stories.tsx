import { useState, ReactNode } from "react";
import { Alert, Button, Stack, Typography } from "@mui/material";
import { Box } from "@mui/system";
import AddBoxIcon from "@mui/icons-material/AddBox";
import {
  Dialog,
  ConfirmationDialog,
  ConfirmDangerActionDialog
} from "../../../components/common/Dialog";

export default {
  title: "Common/Dialog"
};

const OPEN = "OPEN";
const CONFIRMED = "CONFIRMED";
const IDLE = "IDLE";
const CLOSED = "CLOSED";

const loremString =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dignissim aliquam libero, sit amet porttitor felis iaculis a. Nunc consequat urna vitae tellus consequat, a egestas enim dictum. Morbi sodales pharetra sagittis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed velit ex, venenatis id vulputate egestas, molestie id libero. Curabitur eu tincidunt metus. Mauris ultricies tristique quam, ut cursus tortor. Nullam feugiat elementum nulla, nec faucibus lorem sodales quis. Suspendisse sollicitudin sollicitudin sapien, a bibendum dolor rhoncus consectetur. Nunc interdum et turpis vitae vehicula. Donec egestas vitae mauris id scelerisque. Cras suscipit magna nec purus vehicula ultrices. Fusce quam nibh, iaculis in maximus a, ultrices eget tortor. Etiam ex massa, vulputate condimentum fermentum eu, convallis non metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dignissim aliquam libero, sit amet porttitor felis iaculis a. Nunc consequat urna vitae tellus consequat, a egestas enim dictum. Morbi sodales pharetra sagittis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed velit ex, venenatis id vulputate egestas, molestie id libero. Curabitur eu tincidunt metus. Mauris ultricies tristique quam, ut cursus tortor. Nullam feugiat elementum nulla, nec faucibus lorem sodales quis. Suspendisse sollicitudin sollicitudin sapien, a bibendum dolor rhoncus consectetur. Nunc interdum et turpis vitae vehicula. Donec egestas vitae mauris id scelerisque. Cras suscipit magna nec purus vehicula ultrices. Fusce quam nibh, iaculis in maximus a, ultrices eget tortor. Etiam ex massa, vulputate condimentum fermentum eu, convallis non metus.";

interface CustomTitleProps {
  title: string | ReactNode;
}

const CustomTitle = ({ title }: CustomTitleProps) => {
  return (
    <Stack
      flexDirection="row"
      gap={1}
      alignItems="center"
    >
      <AddBoxIcon fontSize="large" />
      <Typography
        variant="button"
        sx={{ verticalalign: "center" }}
      >
        {title}
      </Typography>
    </Stack>
  );
};

interface CustomContentProps {
  content: string | ReactNode;
}

const CustomContent = ({ content }: CustomContentProps) => {
  return <Typography fontFamily="monospace">{content}</Typography>;
};

interface DialogProps {
  title: string | ReactNode;
  titleIsCustomComponent: boolean;
  content: string | ReactNode;
  contentIsCustomComponent: boolean;
}

export const BasicDialog = ({
  title,
  titleIsCustomComponent,
  content,
  contentIsCustomComponent
}: DialogProps) => {
  const [state, setState] = useState(OPEN);

  return (
    <>
      <Button
        variant="contained"
        onClick={() => setState(OPEN)}
      >
        Show Dialog
      </Button>
      <Dialog
        DialogProps={{ open: state === OPEN }}
        onClose={() => setState(IDLE)}
        title={titleIsCustomComponent ? <CustomTitle title={title} /> : title}
        content={
          contentIsCustomComponent ? (
            <CustomContent content={content} />
          ) : (
            content
          )
        }
      />
    </>
  );
};
BasicDialog.args = {
  title: "Some Title",
  titleIsCustomComponent: false,
  content: loremString,
  contentIsCustomComponent: false,
  DialogProps: { fullWidth: true, maxWidth: "md" }
};

const ConfirmationTemplate = ({
  title,
  titleIsCustomComponent,
  content,
  contentIsCustomComponent
}: DialogProps) => {
  const [state, setState] = useState(OPEN);
  const onConfirm = () => setState(CONFIRMED);
  const onClose = () => setState(CLOSED);

  return (
    <>
      <Box sx={{ width: "30%" }}>
        <Button
          fullWidth
          variant="contained"
          onClick={() => setState(OPEN)}
        >
          Open Dialog
        </Button>
        {state === CONFIRMED && (
          <Alert severity="success">You confirmed the dialog!</Alert>
        )}
        {state === CLOSED && (
          <Alert severity="error">You closed the dialog!</Alert>
        )}
      </Box>
      <ConfirmationDialog
        {...{ onConfirm, onClose }}
        DialogProps={{ open: state === OPEN }}
        title={titleIsCustomComponent ? <CustomTitle title={title} /> : title}
        cancelText="Cancel"
        confirmText="Confirm"
        content={
          contentIsCustomComponent ? (
            <CustomContent content={content} />
          ) : (
            content
          )
        }
      />
    </>
  );
};

interface ConfirmationDialogProps extends DialogProps {
  confirmText: string;
  cancelText: string;
  isConfirmDisabled: boolean;
}

export const Confirmation = (args: ConfirmationDialogProps) => (
  <ConfirmationTemplate {...args} />
);
Confirmation.args = {
  ...BasicDialog.args,
  confirmText: "Confirm",
  cancelText: "Cancel",
  isConfirmDisabled: false
};

interface DangerActionDialogProps extends ConfirmationDialogProps {
  valueToCheck: string;
}

const DangerActionComponent = ({
  title,
  content,
  confirmText,
  valueToCheck,
  ...props
}: DangerActionDialogProps) => {
  const [state, setState] = useState(IDLE);

  return (
    <>
      <Box sx={{ width: "30%" }}>
        <Button
          fullWidth
          variant="contained"
          onClick={() => setState(OPEN)}
        >
          Open Danger Action Dialog
        </Button>
        {state === CONFIRMED ? (
          <Alert severity="success">You confirmed a danger action!</Alert>
        ) : null}
      </Box>
      <ConfirmDangerActionDialog
        title={title}
        content={content}
        confirmText={confirmText}
        valueToCheck={valueToCheck}
        DialogProps={{ open: state === OPEN }}
        onConfirm={() => setState(CONFIRMED)}
        onClose={() => setState(CLOSED)}
        {...props}
      />
    </>
  );
};

export const DangerActionDialog = ({
  title,
  content,
  confirmText,
  valueToCheck,
  ...args
}: DangerActionDialogProps) => (
  <DangerActionComponent
    title={title}
    content={content}
    confirmText={confirmText}
    valueToCheck={valueToCheck}
    {...args}
  />
);
DangerActionDialog.args = {
  title: "Delete IOC type",
  content:
    " Type in the complete IOC type name to confirm that you really want to delete the IOC type.",
  confirmText: "Delete",
  valueToCheck: "ec234-67hdj"
};

const getCustomComponent = () => (
  <>
    <Typography
      fontSize="small"
      fontWeight="bold"
      sx={{ marginBottom: "0.5rem" }}
    >
      Note that this IOC type has instances connected, which also would be
      deleted.
    </Typography>
    <Typography>
      Type in the complete IOC type name to confirm that you really want to
      delete the IOC type. This action will delete all instances and operations.
    </Typography>
  </>
);

export const DangerActionDialogWithCustomContent = ({
  title,
  content,
  confirmText,
  valueToCheck,
  ...args
}: DangerActionDialogProps) => (
  <DangerActionComponent
    title={title}
    content={content}
    confirmText={confirmText}
    valueToCheck={valueToCheck}
    {...args}
  />
);

DangerActionDialogWithCustomContent.args = {
  ...DangerActionDialog.args,
  content: getCustomComponent()
};
