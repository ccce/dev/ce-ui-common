import { Button, Paper } from "@mui/material";
import { BrowserRouter } from "react-router-dom";
import { ErrorPage } from "../../../components/common/ErrorPage";
import { theme } from "../../../style/Theme";
import {
  SidewaysMascot,
  VerticalMascot
} from "../../../components/common/Mascot/Mascot";

export default {
  title: "Common/ErrorPage",
  component: ErrorPage
};

const Template = (props) => {
  return (
    <BrowserRouter>
      <Paper sx={{ padding: 2 }}>
        <ErrorPage {...props} />
      </Paper>
    </BrowserRouter>
  );
};

const mascotDefaultProps = { width: "250px", height: "500px" };

export const Default = (args) => <Template {...args} />;
Default.args = {
  title: "Badger Ducks Summoned",
  subtitle: "HTTP 503",
  mascotVariantL: <SidewaysMascot {...mascotDefaultProps} />,
  mascotVariantR: <VerticalMascot {...mascotDefaultProps} />,
  supportHref:
    "https://jira.esss.lu.se/plugins/servlet/desk/portal/44?requestGroup=137",
  details: `Exception in thread "QuantumUniverseThread" com.physics.particles.ParticleCollisionException: Whoa, it seems particles are having a wild party and forgot to invite the laws of physics!
  at com.physics.particles.ParticleSimulator.collideParticles(ParticleSimulator.java:214)
  at com.physics.particles.QuantumCollider.collideInQuantumSpace(QuantumCollider.java:378)
  at com.physics.particles.ParticleAccelerator.runExperiment(ParticleAccelerator.java:765)
  at com.physics.particles.ParticlePhysicsLab.startExperiment(ParticlePhysicsLab.java:512)
  at com.physics.particles.ParticlePhysicsLabMain.main(ParticlePhysicsLabMain.java:32)

Caused by: com.physics.particles.SpacetimeAnomalyException: Space-time continuum is bending over backward trying to accommodate this absurd collision!
  at com.physics.particles.QuantumCollider.calculateQuantumCollision(QuantumCollider.java:276)
  at com.physics.particles.ParticleSimulator.collideParticles(ParticleSimulator.java:207)
  ... 4 more

Suppressed: com.physics.particles.NerfQuantumMechanicsException: Quantum mechanics just had a meltdown. It's not supposed to be this chaotic!
  at com.physics.particles.QuantumCalculator.calculateQuantumState(QuantumCalculator.java:592)
  at com.physics.particles.QuantumCollider.calculateQuantumCollision(QuantumCollider.java:251)
  ... 5 more

Suppressed: com.physics.particles.GravityDefyingException: Oh, no! Some particles decided gravity isn't their thing anymore.
  at com.physics.particles.ParticleSimulator.overrideGravityForParticles(ParticleSimulator.java:348)
  at com.physics.particles.ParticleSimulator.collideParticles(ParticleSimulator.java:201)
  ... 9 more

Suppressed: com.physics.particles.AntiMatterMishapException: Anti-matter particles got loose and are partying with matter particles. Chaos ensued!
  at com.physics.particles.ParticleSimulator.handleAntiMatterInteractions(ParticleSimulator.java:432)
  at com.physics.particles.ParticleSimulator.collideParticles(ParticleSimulator.java:193)
  ... 12 more

Suppressed: com.physics.particles.HeisenbergUncertaintyOverflowException: Heisenberg's principle seems broken. Now particles are certain about everything!
  at com.physics.particles.QuantumCalculator.calculateQuantumState(QuantumCalculator.java:507)
  at com.physics.particles.QuantumCollider.calculateQuantumCollision(QuantumCollider.java:251)
  ... 17 more

Suppressed: com.physics.particles.PauliExclusionFreakoutException: Pauli exclusion principle ran away screaming from the pandemonium of identical particles.
  at com.physics.particles.ParticleSimulator.handleIdenticalParticleBehavior(ParticleSimulator.java:614)
  at com.physics.particles.ParticleSimulator.collideParticles(ParticleSimulator.java:188)
  ... 23 more
`
};

export const Customized = (args) => <Template {...args} />;
Customized.args = {
  ...Default.args,
  titleProps: { variant: "h1", color: theme.palette.error.main },
  subtitleProps: { variant: "h2", fontStyle: "italic" },
  SecondaryActionComponent: (
    <Button
      variant="contained"
      color="error"
      onClick={() => alert("Kablooey!")}
    >
      Click to explode!
    </Button>
  )
};
