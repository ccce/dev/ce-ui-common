import { BugReport, AddBoxOutlined } from "@mui/icons-material";
import { ButtonAppBar } from "../../../components/navigation/ButtonAppBar";
import { IconMenuButton } from "../../../components/common/IconMenuButton";

import { sbDisabledArg } from "../../utils/common-args";

export default {
  title: "Common/IconMenuButton",
  argTypes: {
    iconMenuButton: sbDisabledArg
  }
};

export const SingleButton = (args) => <ButtonAppBar {...args} />;
const singleButtonProps = {
  id: "single-story-",
  menuItems: [
    {
      text: "New IOC Type",
      action: () => alert("New IOC Type")
    }
  ]
};
SingleButton.args = {
  iconMenuButton: <IconMenuButton {...singleButtonProps} />
};

export const MenuButton = (args) => <ButtonAppBar {...args} />;
const menuButtonProps = {
  id: "menu-story-",
  icon: <AddBoxOutlined />,
  menuItems: [
    {
      text: "New IOC Type",
      action: () => alert("New IOC Type")
    },
    {
      text: "Other Action",
      action: () => alert("Some other action"),
      icon: <BugReport />
    }
  ]
};
MenuButton.args = {
  iconMenuButton: <IconMenuButton {...menuButtonProps} />
};
