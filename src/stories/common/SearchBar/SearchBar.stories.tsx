import { useState, useEffect } from "react";
import { Container, Paper } from "@mui/material";
import { SearchBar } from "../../../components/common/SearchBar";
import { Table } from "../../../components/common/Table";
import { usePagination } from "../../../hooks/usePagination";
import { columns } from "../../utils/common-data";
import { sbDisabledArg } from "../../utils/common-args";
import hosts from "../../../mocks/fixtures/api/ListHosts.json";

export default {
  title: "Common/SearchBar"
};

const fuzzySearch = (searchTerm: string) => {
  const lowerCaseSearchTerm = searchTerm.toLowerCase();
  return hosts.netBoxHosts.filter((item) => {
    return Object.values(item).some((value) =>
      String(value).toLowerCase().includes(lowerCaseSearchTerm)
    );
  });
};

const Harness = (args: Record<string, unknown>) => {
  const [rows, setRows] = useState<Record<string, unknown>[]>();
  const [query, setQuery] = useState();
  const [loading, setLoading] = useState(false);
  const { pagination, setPagination } = usePagination({
    initPage: 0,
    initLimit: 20,
    rowsPerPageOptions: [5, 10, 20, 50]
  });

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  }, []);

  useEffect(() => {
    if (query) {
      setRows(fuzzySearch(query));
    } else {
      setRows(hosts.netBoxHosts);
    }
  }, [query]);

  return (
    <SearchBar
      search={setQuery}
      loading={loading}
    >
      <Table
        {...{
          ...args,
          rows,
          onPage: setPagination,
          pagination
        }}
      />
    </SearchBar>
  );
};

export const Default = (args: Record<string, unknown>) => (
  <Container sx={{ display: "flex" }}>
    <Paper sx={{ display: "flex" }}>
      <Harness {...args} />
    </Paper>
  </Container>
);
Default.args = {
  columns
};
Default.argTypes = {
  columns: sbDisabledArg,
  rowClassName: sbDisabledArg
};
