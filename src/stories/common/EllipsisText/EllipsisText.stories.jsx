import { Paper, Typography } from "@mui/material";
import { MemoryRouter } from "react-router-dom";
import { EllipsisText } from "../../../components/common/EllipsisText";
import { ExternalLink, InternalLink } from "../../../components/common/Link";

export default {
  title: "Common/EllipsisText",
  component: EllipsisText
};

const Container = ({ children }) => {
  return (
    <Paper sx={{ height: "100vh", padding: 2 }}>
      <Typography>The following text is too long:</Typography>
      <Paper
        sx={{ padding: 1, width: "190px" }}
        variant="outlined"
      >
        {children}
      </Paper>
      <Typography
        variant="body2"
        fontStyle="italic"
      >
        (hover over it to view!)
      </Typography>
    </Paper>
  );
};

const TextTemplate = ({ text, ...props }) => {
  return (
    <Container>
      <EllipsisText {...props}>{text}</EllipsisText>
    </Container>
  );
};

export const Default = (args) => <TextTemplate {...args} />;
Default.args = {
  text: "There's some really important information here but it is obscured!",
  TypographyProps: {
    variant: "body2"
  },
  disableTooltip: false,
  TooltipProps: {
    placement: "right-end"
  }
};

const ExternalLinkTemplate = ({ text, ...props }) => {
  return (
    <Container>
      <ExternalLink
        newTab
        href="https://europeanspallationsource.se/"
        label={text}
      >
        <EllipsisText {...props}>{text}</EllipsisText>
      </ExternalLink>
    </Container>
  );
};

export const ExternalLinkEllipsisText = (args) => (
  <ExternalLinkTemplate {...args} />
);
ExternalLinkEllipsisText.args = {
  ...Default.args
};

const InternalLinkTemplate = ({ text, ...props }) => {
  return (
    <MemoryRouter>
      <Container>
        <InternalLink
          to="/"
          label={text}
        >
          <EllipsisText {...props}>{text}</EllipsisText>
        </InternalLink>
      </Container>
    </MemoryRouter>
  );
};

export const InternalLinkEllipsisText = (args) => (
  <InternalLinkTemplate {...args} />
);
InternalLinkEllipsisText.args = {
  ...Default.args
};
