import { Autocomplete } from "../../components/common/Autocomplete";

export default {
  title: "Fields/Autocomplete"
};

const data = [
  {
    name: "Fido",
    type: "dog",
    age: 2
  },
  {
    name: "Oscar",
    type: "cat",
    age: 3.1
  },
  {
    name: "Bud",
    type: "dog",
    age: 0.5
  }
];

const Template = ({ loading, ...args }) => {
  return (
    <Autocomplete
      id="my-autocomplete-input"
      loading={loading}
      autocompleteProps={{
        options: data,
        getOptionLabel: (option) => {
          return option?.name;
        },
        onChange: (event, value, reason) => {
          console.debug({ onChange: { event, value, reason } });
        },
        onInputChange: (event, value, reason) => {
          console.debug({ onChange: { event, value, reason } });
        }
      }}
      textFieldProps={{
        label: "My Autocomplete Input",
        variant: "outlined",
        required: true
      }}
      {...args}
    />
  );
};

export const Default = (args) => <Template {...args} />;

export const Loading = (args) => (
  <Template
    loading
    {...args}
  />
);
