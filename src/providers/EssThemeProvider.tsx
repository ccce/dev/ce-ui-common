import { ReactNode } from "react";
import { createTheme } from "@mui/material/styles";
import { CssBaseline, ThemeProvider, Theme } from "@mui/material";
import { theme as essTheme } from "../style/Theme";

interface EssThemeProviderProps {
  theme: Theme;
  children: ReactNode;
}

export const EssThemeProvider = ({
  theme,
  children
}: EssThemeProviderProps) => {
  const customTheme = createTheme(essTheme, theme ?? {});

  return (
    <ThemeProvider theme={customTheme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
};
