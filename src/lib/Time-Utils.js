import dayjs, { extend } from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import duration from "dayjs/plugin/duration";

extend(relativeTime);
extend(duration);

/**
 * Converts date string into ISO 8601 date-time format
 * @param {*} value date value from server
 * @returns date and time string in ISO 8601 format
 */
export const formatDateAndTime = (value) => {
  if (dayjs(value).isValid()) {
    return dayjs(value).format("YYYY-MM-DD HH:mm:ss");
  }
  return "Invalid date";
};

export const formatRelativeTime = (value) => {
  if (dayjs(value).isValid()) {
    return dayjs(value).fromNow();
  }
  return "Invalid date";
};

export const formattedDuration = ({ startDate, finishDate }) => {
  if (dayjs(startDate).isValid() && dayjs(finishDate).isValid()) {
    return dayjs
      .duration(finishDate.getTime() - startDate.getTime())
      .format("HH:mm:ss");
  } else {
    return "Invalid date";
  }
};

export const dateFormat = () => {
  return "YYYY-MM-DD";
};
